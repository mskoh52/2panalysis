#!/bin/bash

folders=(
2016/02.February/160222_datcre_chrmcherry_thy1gcamp3
)

export folders

if [ $(hostname) == "imagek-marks" ]
then
    export PYENV_VERSION="anaconda3-2.3.0"
    export DATADIR="/mnt/data/Photonstrobe"
    export HFILEDIR="/mnt/data/Photonstrobe"
    export OUTPUTDIR="/home/mkoh/Documents/analysis/2pAnalysis/completed_analysis"
else
    export PYENV_VERSION="anaconda3-2.4.1"
    export DATADIR="/run/media/mkoh/MKoh_Data/Photonstrobe"
    export HFILEDIR="/home/mkoh/Documents/lab/data/Photonstrobe"
    export OUTPUTDIR="/home/mkoh/Documents/lab/analysis/2pAnalysis/completed_analysis"
fi
