#!/bin/bash

for n in ${!folders[@]}
do
    python code/glomstim_analysis.py ${folders[n]}
done

wait

echo "=========== Done!!! ==========="
