#!/bin/bash

for n in ${!folders[@]}
do
    # don't background these processes, because registration uses multiprocessing
    python -O code/registration_chunked.py ${folders[n]}
done

wait

echo "=========== Done!!! ==========="

