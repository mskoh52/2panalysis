#%run -ni init.py
sys.path.append('/home/mkoh/Documents/code/plotstreamer')
from plotstreamer import MatplotlibChannel
from io import BytesIO
import base64

pub_port = 'tcp://127.0.0.1:10000'
sub_port = 'tcp://127.0.0.1:10002'

c = MatplotlibChannel('plot_001', pub_port, sub_port)
d = MatplotlibChannel('plot_002', pub_port, sub_port)
e = MatplotlibChannel('plot_003', pub_port, sub_port)

##
# For 160308
#odor_expt = Experiment(hfile['fov3/odors_FOV_0'])
#light_expt = Experiment(hfile['fov6/odors_rois_p25_t2_FOV_0'])
# For 160222
odor_expt = Experiment(hfile['fov1/odors_t2_FOV_0'])
light_expt = Experiment(hfile['fov3/odors_rois_p25_t3_FOV_0'])

##
# replace the get_label function in each roi
replace_get_label = True
if replace_get_label:
    import readIJrois
    from functools import partial
    for r in odor_expt.rois+light_expt.rois:
        p = partial(readIJrois.Roi.get_label, r)
        r.get_label = p

odorfig, odorax = odor_expt.display_rois()
lightfig, lightax = light_expt.display_rois()

##
c.sendfigure(odorfig)
d.sendfigure(lightfig)

##
# List of matching rois
# For each element in light_expt.rois, what is the corresponding element in
# odor_expt.rois

# For 160308
#roi_matches = [0, 2, 1, 33, 26, 28, 27, -1, 31, -1, -1, -1, -1, 3, 6, 5, 8, 12,
#               10, 14, 15, 13, -1, 19, -1, 20, 21, 34]
# For 160222
roi_matches = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,-1,18,19,20,21,22,23,
               -1,-1,27,28,29,30,31,32,33,34,35,36,37,38,39,-1,-1,-1,-1,-1,40,-1,-1]

# set primary stim to odor so that it uses odor baseline to calculate df/f
odor_expt.settings['Primary stimulus'] = 'odor'
light_expt.settings['Primary stimulus'] = 'odor'

odor_trace = dff_by_condition(odor_expt)
light_trace = dff_by_condition(light_expt)

n = 1
#fig, ax = plt.subplots(1,1)
#ax.plot(np.nanmean(light_trace[n], axis=1).T)
def filter_rois(roi):
    def _filt(data):
        return(data[roi,:,:])
    return _filt

fig, ax, resp = plotting.Experiment.plot_condition_traces(light_expt,
                                                          condition_selection=(2,5,8),
                                                          subplotshape=(3,1))#,
                                                          #plot_filter = filter_rois(strong_rois))
c.sendfigure(fig)

#For 160308
#no_light = np.nanmean(light_trace[4:7], axis=2)
#light_response = light_trace[7:] - np.concatenate((light_trace[4:7], light_trace[4:7],light_trace[4:7]), axis=0)
#light_response_stims = light_expt.unique_conditions[7:]
#For 160222
no_light = np.nanmean(odor_trace[(2,5,8),:], axis=2)
#no_light = np.nanmean(light_trace[(6,7,8),:], axis=2)
light_response = np.full(light_trace.shape, np.nan)
for r in range(len(light_expt.rois)):
    for co,n in zip(range(len(light_expt.unique_conditions)), ((0,1,2)*3)):
        if roi_matches[r] > 0:
            light_response[co,r,:,:] = light_trace[co,r,:,:] - no_light[n, roi_matches[r],:]
        else:
            light_response[co,r,:,:] = light_trace[co,r,:,:]
light_response_stims = light_expt.unique_conditions

fig, ax = plt.subplots(1,1)
ax.plot(np.mean(np.nanmean(light_trace[0], axis=1)[:,110:120], axis=1))
d.sendfigure(fig)
strong_rois = np.argwhere(np.mean(np.nanmean(light_trace[2], axis=1)[:,110:120], axis=1) > .2).flatten()

fig, ax = plt.subplots(3,3)
ax = ax.flatten()
roi=strong_rois[0]
for a,lr,lrs in zip(ax, light_response, light_response_stims):
    a.plot(np.nanmean(lr[roi],axis=0).T, 'k', linewidth=3)
    a.hold(True)
    a.plot(lr[roi].T, 'b:')
    a.set_title(lrs[1])
fig.tight_layout()
fig.suptitle('ROI {0}'.format(roi))
c.sendfigure(fig)

fig, ax = plt.subplots(3,1)
colors = plt.cm.jet_r(np.linspace(0,1,len(strong_rois[:3])))
for a,co in zip(ax,(2,5,8)):
    for m,r in enumerate(strong_rois[:3]):
        a.hold(True)
        a.plot(np.nanmean(light_trace[co,r,:,:],axis=0).T, color = colors[m], linestyle='-', linewidth=2.5)
        a.plot(light_trace[co,r,:,:].T, color = colors[m], linestyle=':')
        a.set_xlim(85, 175)
        a.plot((139,139), (-.2,1), 'b-')
fig.set_size_inches(10,7.5)
d.sendfigure(fig)

fig, ax = plt.subplots(3,1)
colors = plt.cm.jet_r(np.linspace(0,1,len(strong_rois)))
for a,co in zip(ax,(3,5,9)):
    for m,r in enumerate(strong_rois):
        a.hold(True)
        a.plot(np.nanmean(odor_trace[co,roi_matches[r],:,:],axis=0).T, color = colors[m], linestyle='-', linewidth=2.5)
        a.plot(odor_trace[co,roi_matches[r],:,:].T, color = colors[m], linestyle=':')
        a.set_xlim(85, 175)
fig.set_size_inches(10,7.5)
c.sendfigure(fig)

ors = np.zeros( (len(odor_expt.unique_conditions), len(strong_rois[:3])) )
for o in range(len(odor_expt.unique_conditions)):
    for r in range(len(strong_rois[:3])):
        ors[o,r] = np.nanmean(odor_trace[o,r,:,100:150])

fig, ax = plt.subplots(3,1)
for r,a in enumerate(ax):
    a.bar(range(len(odor_expt.unique_conditions)), ors[:,r], facecolor='k')
c.sendfigure(fig)

lrs = np.zeros( (len(light_expt.unique_spots),len(strong_rois[:3])) )
for ll,l in enumerate((2,5,8)):
    for r in range(len(strong_rois[:3])):
        lrs[ll,r] = np.nanmean(light_trace[l,r,:,139:150]) - np.nanmean(light_trace[l,r,:,139])

fig, ax = plt.subplots(3,1)
for r,a in enumerate(ax):
    a.bar(range(len(light_expt.unique_spots)), lrs[:,r], facecolor='b')
e.sendfigure(fig)

from scipy.spatial.distance import pdist, squareform
light_dist = squareform(pdist(lrs.T))
odor_dist = squareform(pdist(ors.T))

fig, ax = plt.subplots(1,2)
im=ax[0].imshow(light_dist, interpolation='none', vmin=0, vmax=.75, cmap=plt.cm.CMRmap)
im=ax[1].imshow(odor_dist, interpolation='none', vmin=0, vmax=.75, cmap=plt.cm.CMRmap)
fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax)
d.sendfigure(fig)

ld = pdist(lrs.T)
od = pdist(ors.T)
X = np.vstack( ((1,1,1), ld)).T
m,resid,_,_ = np.linalg.lstsq(X, od[None,:].T)
from scipy.stats import pearsonr
corr,p = pearsonr(ld,od)

fig, ax = plt.subplots(1,1)
ax.scatter(ld,od)
xl = ax.get_xlim()
yl = ax.get_ylim()
line = m[0] + xl*m[1]
ax.plot(xl, line, 'r')
ax.text( .1, .2, 'r = {0:.02f}\np = {1:.02f}'.format(corr, p))
ax.set_xlabel('Light response distance')
ax.set_ylabel('Odor response distance')
ax.set_xlim(xl)
ax.set_ylim(yl)
c.sendfigure(fig)

def dff_by_condition(self):
    dff = self.dff
    responses = np.full((len(self.unique_conditions), len(self.rois), max(self.condition_count), dff.shape[2]), np.nan)
    for c in range(len(self.unique_conditions)):
        stim = self.unique_conditions[c]
        condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
        this_cond_response = self.dff[:, condition_mask, :]
        responses[c,:,:this_cond_response.shape[1],:] = this_cond_response
    return responses
