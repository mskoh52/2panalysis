## Load
hfiledir = '/home/mkoh/Documents/lab/data/Photonstrobe/'
datadir = '/run/media/mkoh/MKoh_Data/Photonstrobe'
exptdir = '2016/02.February/160222_datcre_chrmcherry_thy1gcamp3'
outputdir = '/home/mkoh/Documents/lab/analysis/2pAnalysis/completed_analysis'

import matplotlib
matplotlib.use('agg')

import os
from glob import glob
import sys
sys.path.append('/home/mkoh/Documents/lab/analysis/2pAnalysis/code/')

import numpy as np
import h5py

from plotting import *
from image_anal_helpers import *

import matplotlib.pyplot as plt

hfile = h5py.File(os.path.join(hfiledir, exptdir+'.hdf5'), 'r')

fovs = list(hfile.keys())
hfile.visit(treeprint(1))
print('---------------------------\n-----------NOTES-----------\n---------------------------')
with open(os.path.join(datadir,exptdir,'notes.txt'), 'r') as f:
    print(f.read())
with open(os.path.join(outputdir,exptdir,'notes.txt'), 'r') as f:
    print(f.read())

import pickle

with open('/home/mkoh/Documents/lab/analysis/2pAnalysis/completed_analysis/2015/09.September/150922_beads/px_per_15um_400x400', 'rb') as fid:
    final = pickle.load(fid)

from scipy.optimize import curve_fit
popt, pcov = curve_fit(lambda x,k: k*(1/x), np.array((1.,2.,3.,4.,5.)), final)
um_per_pixel = lambda zoom, res: 7.5/(popt[0]*(1/zoom))*(400/res)
