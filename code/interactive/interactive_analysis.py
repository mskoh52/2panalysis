## Load
hfiledir = '/home/mkoh/Documents/lab/data/Photonstrobe/'
datadir = '/run/media/mkoh/MKoh_Data/Photonstrobe'
exptdir = '2016/03.March/160308_datcre_chrmcherry_thy1gcamp3'
outputdir = '/home/mkoh/Documents/lab/analysis/2pAnalysis/completed_analysis'

import matplotlib
matplotlib.use('agg')

import os
from glob import glob
import sys
sys.path.append('/home/mkoh/Documents/lab/analysis/imaging/code/')

import numpy as np
import h5py

%run -ni code/plotting.py
%run -ni code/image_anal_helpers.py

import matplotlib.pyplot as plt

hfile = h5py.File(os.path.join(hfiledir, exptdir+'.hdf5'), 'r')

fovs = list(hfile.keys())
hfile.visit(treeprint(1))
print('---------------------------\n-----------NOTES-----------\n---------------------------')
!cat $datadir$exptdir/notes.txt
!cat $outputdir$exptdir/analysis_notes.txt

import pickle

with open('/home/mkoh/Documents/lab/analysis/2pAnalysis/completed_analysis/2015/09.September/150922_beads/px_per_15um_400x400', 'rb') as fid:
    final = pickle.load(fid)

from scipy.optimize import curve_fit
popt, pcov = curve_fit(lambda x,k: k*(1/x), np.array((1.,2.,3.,4.,5.)), final)
um_per_pixel = lambda zoom, res: 7.5/(popt[0]*(1/zoom))*(400/res)

sys.path.append('/home/mkoh/Documents/code/plotstreamer')
from plotstreamer import Channel
from io import BytesIO
import base64

pub_port = 'tcp://127.0.0.1:10000'
sub_port = 'tcp://127.0.0.1:10002'

c = Channel('plot_001', pub_port, sub_port)
d = Channel('plot_002', pub_port, sub_port)
e = Channel('plot_003', pub_port, sub_port)

def sendfigure(channel, fig):
    out = BytesIO()
    fig.savefig(out, dpi=100)
    channel.send_data(base64.b64encode(out.getvalue()))

## FOV 1
fov = hfile[fovs[2]]
sets, _, _ = summarize_fov(fov, datadir, exptdir)

set_ = fov[sets[0]]
expt = Experiment(set_)

expt.settings['ROI set'] = 'gridrois'
fig, ax, resp = expt.display_roi_responses(print_roi_numbers=False)

sendfigure(c,fig)

fig, ax, resp = expt.display_roi_responses(normalization='all')
sendfigure(c,fig)

def filt(ca):
    return ca[:29]
fig, ax, resp = expt.plot_condition_traces(condition_selection = None, plot_filter=filt)
sendfigure(d,fig)
plt.close('all')

fig, ax, resp = expt.plot_condition_traces(condition_selection = (1,4,7))
sendfigure(e,fig)
plt.close('all')

# analyze responses
condition_selection = tuple(range(len(expt.unique_conditions)))
responses = np.zeros( (9 , 47, 5, 249) )
for c in range(len(expt.unique_conditions)):
    stim = expt.unique_conditions[c]
    condition_mask = np.array([np.array_equal(x, stim[0]) for x in expt.odors]) * np.array([np.array_equal(x, stim[1]) for x in expt.spots])
    responses[c] = expt.dff[:, np.atleast_1d(np.argwhere(condition_mask).squeeze()), :]

fig, ax = plt.subplots(1,2)
ax[0].plot(responses[2].mean(axis=1)[:,expt.light_response_period[0]:expt.light_response_period[1]].T)
ax[1].plot(responses[5].mean(axis=1)[:,expt.light_response_period[0]:expt.light_response_period[1]].T)
sendfigure(e, fig)

## 160224
fovs = list(hfile_24.keys())
fov = hfile_24[fovs[3]]
sets,_,_ = summarize_fov(fov,datadir,exptdir)

set_ = fov[sets[0]]
expt_24_f3_s0 = Experiment(set_)
