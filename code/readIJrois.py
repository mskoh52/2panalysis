# TODO:
# check if type checking in RoiDecoder.__init__ is done the right way
# check if type checking in try except clauses is done the right way
# implement is_interior_point for all other roi types

class RoiDecoder:
    def __init__(self, image_size):
        self.image_size = tuple([int(x) for x in image_size])

    def fromfile(self, fname, fileformat='imagej'):
        """ Load rois from a file

        Parameters
        ----------
        fname : str
        fileformat : {'imagej', 'labview'}

        Returns
        -------
        roi or rois
            If fileformat == 'imagej', return a single roi
            If fileformat == 'labview', return array of rois
        """
        if fileformat == 'imagej':
            with open(fname, 'rb') as f:
                roi = self.frombytes(fname, f.read())
            return roi
        elif fileformat == 'labview':
            with open(fname, 'rb') as f:
                rois = self.frombytes(fname, f.read(), fileformat='labview')
            return rois

    def fromzip(self, fname):
        from zipfile import ZipFile
        with ZipFile(fname) as zf:
            rois = [self.frombytes(name, zf.read(name)) for name in zf.namelist()]
        return rois

    def frombytes(self, name, bytez, fileformat='imagej'):
        from struct import unpack
        if fileformat == 'imagej':
            def getByte(offset):
                return unpack('>b', bytez[offset:offset+1])[0]
            def getShort(offset):
                return unpack('>h', bytez[offset:offset+2])[0]
            def getInt(offset):
                return unpack('>i', bytez[offset:offset+4])[0]
            def getFloat(offset):
                return unpack('>f', bytez[offset:offset+4])[0]

            # offsets
            VERSION_OFFSET = 4
            TYPE = 6
            TOP = 8
            LEFT = 10
            BOTTOM = 12
            RIGHT = 14
            N_COORDINATES = 16
            X1 = 18
            Y1 = 22
            X2 = 26
            Y2 = 30
            XD = 18
            YD = 22
            WIDTHD = 26
            HEIGHTD = 30
            STROKE_WIDTH = 34
            SHAPE_ROI_SIZE = 36
            STROKE_COLOR = 40
            FILL_COLOR = 44
            SUBTYPE = 48
            OPTIONS = 50
            ARROW_STYLE = 52
            ELLIPSE_ASPECT_RATIO = 52
            POINT_TYPE= 52
            ARROW_HEAD_SIZE = 53
            ROUNDED_RECT_ARC_SIZE = 54
            POSITION = 56
            HEADER2_OFFSET = 60
            COORDINATES = 64

            # header2 offsets
            C_POSITION = 4
            Z_POSITION = 8
            T_POSITION = 12
            NAME_OFFSET = 16
            NAME_LENGTH = 20
            OVERLAY_LABEL_COLOR = 24
            OVERLAY_FONT_SIZE = 28
            AVAILABLE_BYTE1 = 30
            IMAGE_OPACITY = 31
            IMAGE_SIZE = 32
            FLOAT_STROKE_WIDTH = 36
            ROI_PROPS_OFFSET = 40
            ROI_PROPS_LENGTH = 44

            # types
            POLYGON = 0
            RECT = 1
            OVAL = 2
            LINE = 3
            FREELINE = 4
            POLYLINE = 5
            NOROI = 6
            FREEHAND = 7
            TRACED = 8
            ANGLE = 9
            POINT = 10

            # subtypes
            TEXT = 1
            ARROW = 2
            ELLIPSE = 3
            IMAGE = 4

            # options
            SPLINE_FIT = 1
            DOUBLE_HEADED = 2
            OUTLINE = 4
            OVERLAY_LABELS = 8
            OVERLAY_NAMES = 16
            OVERLAY_BACKGROUNDS = 32
            OVERLAY_BOLD = 64
            SUB_PIXEL_RESOLUTION = 128
            DRAW_OFFSET = 256
            ZERO_TRANSPARENT = 512

            try:
                if bytez[0:4].decode() != 'Iout':
                    raise IOError('This is not an ImageJ ROI')
            except UnicodeDecodeError:
                raise IOError('This is not an ImageJ ROI')

            size = len(bytez)

            version = getShort(VERSION_OFFSET)
            roi_type = getByte(TYPE)
            subtype = getShort(SUBTYPE)
            top = getShort(TOP)
            left = getShort(LEFT)
            bottom = getShort(BOTTOM)
            right = getShort(RIGHT)
            width = right - left
            height = bottom - top
            n_coordinates = getShort(N_COORDINATES)
            options = getShort(OPTIONS)
            position = getInt(POSITION)
            hdr2Offset = getInt(HEADER2_OFFSET)
            channel_num = 0
            slice_num = 0
            frame_num = 0
            overlayLabelColor = 0
            overlayFontSize = 0
            imageOpacity = 0
            imageSize = 0
            subPixelResolution = ((options & SUB_PIXEL_RESOLUTION) != 0) and (version >= 222)
            drawOffset = subPixelResolution and ((options & DRAW_OFFSET) != 0)

            subPixelRect = (version >= 223) and subPixelResolution and (roi_type==RECT or OVAL)
            if subPixelRect:
                xd = getFloat(XD)
                yd = getFloat(YD)
                widthd = getFloat(WIDTHD)
                heightd = getFloat(HEIGHTD)

            if (hdr2Offset > 0) and (hdr2Offset+IMAGE_SIZE+4 <= size):
                channel_num = getInt(hdr2Offset+C_POSITION)
                slice_num = getInt(hdr2Offset+Z_POSITION)
                frame_num = getInt(hdr2Offset+T_POSITION)
                overlayLabelColor = getInt(hdr2Offset+OVERLAY_LABEL_COLOR)
                overlayFontSize = getShort(hdr2Offset+OVERLAY_FONT_SIZE)
                imageOpacity = getByte(hdr2Offset+IMAGE_OPACITY)
                imageSize = getInt(hdr2Offset+IMAGE_SIZE)

            name = name[:-4]
            isComposite = getInt(SHAPE_ROI_SIZE) > 0

            if isComposite:
                if roi_type != RECT:
                    raise ValueError('Invalid composite ROI type')
                n_shaperoi = getInt(SHAPE_ROI_SIZE)
                base = COORDINATES
                shapeArray = []
                for i in range(n_shaperoi):
                    shapeArray.append(getFloat(base))
                    base += 4
                roi = ShapeRoi(shapeArray, image_size = self.image_size)
            else:
                if roi_type == RECT:
                    if subPixelRect:
                        roi = RectRoi(xd, yd, widthd, heightd, image_size = self.image_size)
                    else:
                        roi = RectRoi(left, top, width, height, image_size = self.image_size)
                    arcSize = getShort(ROUNDED_RECT_ARC_SIZE)
                    if arcSize > 0:
                        roi._corner_diameter = arcSize
                elif roi_type == OVAL:
                    if subPixelRect:
                        roi = OvalRoi(xd, yd, widthd, heightd, image_size = self.image_size)
                    else:
                        roi = OvalRoi(left, top, width, height, image_size = self.image_size)
                elif roi_type == LINE:
                    x1 = getFloat(X1)
                    y1 = getFloat(Y1)
                    x2 = getFloat(X2)
                    y2 = getFloat(Y2)
                    if subtype == ARROW:
                        roi = ArrowRoi(x1, y1, x2, y2,
                                       double_headed = (options&DOUBLE_HEADED),
                                       outline = (options&OUTLINE),
                                       style = getByte(ARROW_STYLE),
                                       head_size = getByte(ARROW_HEAD_SIZE))
                    else:
                        roi = LineRoi(x1, y1, x2, y2, draw_offset = drawOffset)
                elif (roi_type == POLYGON or
                      roi_type == FREEHAND or
                      roi_type == TRACED or
                      roi_type == POLYLINE or
                      roi_type == FREELINE or
                      roi_type == ANGLE or
                      roi_type == POINT):
                    if n_coordinates == 0:
                        return None

                    if subPixelResolution:
                        p = [(0.0, 0.0)]*n_coordinates
                        base1 = COORDINATES+4*n_coordinates
                        base2 = base1+4*n_coordinates
                        for i in range(n_coordinates):
                            p_temp = (getFloat(base1+i*4), getFloat(base2+i*4))
                            if p_temp[0] < 0:
                                p_temp[0] = 0
                            if p_temp[0] >= self.image_size[1]:
                                p_temp[0] = self.image_size[1]-1
                            if p_temp[1] < 0:
                                p_temp[1] = 0
                            if p_temp[1] >= self.image_size[0]:
                                p_temp[1] = self.image_size[0]-1
                            p[i] = tuple(p_temp)
                    else:
                        p = [(0, 0)]*n_coordinates
                        base1 = COORDINATES
                        base2 = base1+2*n_coordinates
                        for i in range(n_coordinates):
                            p_temp = [getShort(base1+i*2) + left, getShort(base2+i*2) + top]
                            if p_temp[0] < 0:
                                p_temp[0] = 0
                            if p_temp[0] >= self.image_size[1]:
                                p_temp[0] = self.image_size[1]-1
                            if p_temp[1] < 0:
                                p_temp[1] = 0
                            if p_temp[1] >= self.image_size[0]:
                                p_temp[1] = self.image_size[0]-1

                            p[i] = tuple(p_temp)

                    if roi_type == POINT:
                        roi = PointRoi(p, image_size = self.image_size)
                    elif roi_type == POLYGON:
                        roi = PolygonRoi(p, image_size = self.image_size)
                    elif roi_type == FREEHAND:
                        if subtype == ELLIPSE:
                            x1 = getFloat(X1)
                            y1 = getFloat(Y1)
                            x2 = getFloat(X2)
                            y2 = getFloat(Y2)
                            aspectRatio = getFloat(ELLIPSE_ASPECT_RATIO)
                            roi = EllipseRoi(x1, y2, x2, y2, aspectRatio, image_size = self.image_size)
                        else:
                            roi = FreeRoi(p, image_size = self.image_size)
                    elif roi_type == TRACED:
                        roi = TracedRoi(p, image_size = self.image_size)
                    elif roi_type == POLYLINE:
                        roi = PolylineRoi(p, image_size = self.image_size)
                    elif roi_type == FREELINE:
                        roi = FreelineRoi(p, image_size = self.image_size)
                    elif roi_type == ANGLE:
                        roi = AngleRoi(p, image_size = self.image_size)

            return roi

        elif fileformat == 'labview':
            ptr=0
            n_rois = int.from_bytes(bytez[ptr:ptr+4], 'big')
            ptr+=4

            rois = []
            for r in range(n_rois):
                indices = int.from_bytes(bytez[ptr:ptr+4], 'big')
                ptr+=4
                global_rectangle = unpack('>4i', bytez[ptr:ptr+16])
                ptr+=16

                n_contours = int.from_bytes(bytez[ptr:ptr+4], 'big')
                ptr+=4
                contours = []
                for c in range(n_contours):
                    contour_type = int.from_bytes(bytez[ptr:ptr+4], 'big')
                    ptr+=4
                    roi_type = int.from_bytes(bytez[ptr:ptr+4], 'big')
                    ptr+=4
                    n_points = int.from_bytes(bytez[ptr:ptr+4], 'big')
                    ptr+=4
                    points = unpack('>'+str(n_points)+'i', bytez[ptr:(ptr+n_points*4)])
                    ptr+=(n_points*4)
                    if roi_type == 3:
                        contours.append(RectRoi(points[0], points[1], points[2]-points[0], points[3]-points[1], image_size=self.image_size))
                    elif roi_type == 4:
                        contours.append(OvalRoi(points[0], points[1], points[2]-points[0], points[3]-points[1], image_size=self.image_size))
                    elif roi_type == 6:
                        contours.append(FreeRoi(points, image_size = self.image_size))

                if n_contours == 1:
                    roi = contours[0]
                elif n_contours > 1:
                    roi = CompoundRoi(contours, image_size = self.image_size)
                rois.append(roi)

            return rois

class Roi:
    def __init__(self, image_size=(0,0), draw_offset=0):
        import numpy as np

        self.image_size = image_size
        self.draw_offset = 0
        self.mask = np.zeros(image_size, dtype='bool')
    def get_label(self, text, location='center', **kwargs):
        """
        Parameters
        ----------
        location: {'center', 'left', 'right', 'top', 'bottom'})
        text: string
        **kwargs: extra keyword arguments to pass to Text constructor

        Returns
        -------
        matplotlib.text.Text
        """
        from matplotlib.text import Text
        if hasattr(self, 'points'):
            import numpy as np
            x = np.argwhere(self.mask!=0).T[1]
            y = np.argwhere(self.mask!=0).T[0]
            if (len(x) > 0) and (len(y) >0):
                x = np.mean(x)
                y = np.mean(y)
            else:
                from warnings import warn
                warn('Trying to print ROI label "{0}", but ROI is empty'.format(text), category=RuntimeWarning)
                x = 0
                y = 0
        else:
            if location == 'center':
                x = self.x + self.width/2
                y = self.y + self.height/2
            elif location == 'left':
                x = self.x + self.image_size[1]/50
                y = self.y + self.height/2
            elif location == 'right':
                x = self.x + self.width - self.image_size[1]/50
                y = self.y + self.height/2
            elif location == 'top':
                x = self.x + self.width/2
                y = self.y + self.image_size[0]/50
            elif location == 'bottom':
                x = self.x + self.width/2
                y = self.y + self.height - self.image_size[0]/50
        return Text(x=x, y=y, text=text, **kwargs)

class CompoundRoi(Roi):
    def __init__(self, subrois, **kwargs):
        super().__init__(**kwargs)
        self.mask = sum( [ sr.mask for sr in subrois ] )
        self.subrois = subrois
        inds = self.mask.nonzero()
        self.x = inds[1].min()
        self.y = inds[0].min()
        self.width = inds[1].max() - self.x
        self.height = inds[0].max() - self.y
    def get_patch(self, **kwargs):
        return [r.get_patch(**kwargs) for r in self.subrois]

class ShapeRoi(Roi):
    def __init__(self, shape_array, **kwargs):
        import numpy as np

        super().__init__(**kwargs)

        # Emulate java GeneralPath class to find points on shape roi border

        # http://docs.oracle.com/javase/7/docs/api/constant-values.html#java.awt.geom.PathIterator.SEG_CLOSE
        SEG_CLOSE      = 4
        SEG_CUBICTO    = 3
        SEG_LINETO     = 1
        SEG_MOVETO     = 0
        SEG_QUADTO     = 2
        WIND_EVEN_ODD  = 0
        WIND_NON_ZERO  = 1

        # Convert shape_array into list of segments
        segments = []
        while len(shape_array) > 0:
            if shape_array[0] == SEG_CLOSE:
                segments.append([shape_array[0],])
                shape_array = []
            elif (shape_array[0] == SEG_MOVETO) or (shape_array[0] == SEG_LINETO):
                segments.append(shape_array[0:3])
                shape_array = shape_array[3:]
            elif shape_array[0] == SEG_QUADTO:
                segments.append(shape_array[0:5])
                shape_array = shape_array[5:]
            elif shape_array[0] == SEG_CUBICTO:
                segments.append(shape_array[0:7])
                shape_array = shape_array[7:]

        # Process the segments
        import drawing as draw
        from PIL import Image, ImageDraw
        pos = (0.,0.)
        from warnings import warn
        if segments[0][0] != SEG_MOVETO:
            warn('readIJrois warning: First segment is not a moveto command')

        img = Image.new('L', (self.image_size[1], self.image_size[0]), 0)
        imgdraw = ImageDraw.Draw(img)
        self.points = []
        for seg in segments:
            if seg[0] == SEG_MOVETO:
                pos = [0,0]
                pos[0] = max(0, min(seg[2], self.mask.shape[0]-1))
                pos[1] = max(0, min(seg[1], self.mask.shape[1]-1))
                pos = tuple(pos)
                self.mask[pos] = True
                self.points.append(seg[1:])
            elif seg[0] == SEG_LINETO:
                #points = draw.draw_line(pos, seg[1:])
                #self.mask[points] = True
                imgdraw.line([pos, tuple(seg[1:])], fill=1)
                pos = [0,0]
                pos[0] = max(0, min(seg[2], self.mask.shape[0]-1))
                pos[1] = max(0, min(seg[1], self.mask.shape[1]-1))
                pos = tuple(pos)
                self.points.append(seg[1:])
            elif seg[0] == SEG_QUADTO:
                warning('readIJrois: unimplemented shape segment type')
                #points = draw.draw_quad(pos, seg[3:], seg[1:3])
                #pos = tuple(seg[3:])
                #self.mask[points] = True
            elif seg[0] == SEG_CUBICTO:
                warning('readIJrois: unimplemented shape segment type')
                #points = draw.draw_cubic(pos, seg[5:], seg[1:3], seg[3:5])
                #pos = tuple(seg[5:])
                #self.mask[points] = True
            elif seg[0] == SEG_CLOSE:
                imgdraw.line([pos, tuple(segments[0][1:])], fill=1)

        self.mask = draw.fill_outline(np.array(img, dtype='bool'))
        self.x = min([p[0] for p in self.points])
        self.y = min([p[1] for p in self.points])
        self.width = max([p[0] for p in self.points]) - self.x
        self.height = max([p[1] for p in self.points]) - self.y
    def get_patch(self, **kwargs):
        """
        Parameters
        ----------
        **kwargs: extra keyword arguments to pass to Text constructor

        Returns
        -------
        matplotlib.patches.Polygon
        """
        from matplotlib.patches import Polygon
        return Polygon(xy = self.points, **kwargs)

class RectRoi(Roi):
    def __init__(self, x, y, width, height, corner_diameter=0, **kwargs):
        import numpy as np

        super().__init__(**kwargs)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self._corner_diameter = corner_diameter

        mx,my = np.meshgrid(np.arange(self.image_size[1]), np.arange(self.image_size[0]))
        self.mask = ((mx >= self.x) & (mx < self.x + self.width) &
                     (my >= self.y) & (my < self.y + self.height))
    def get_patch(self, **kwargs):
        """
        Parameters
        ----------
        **kwargs: extra keyword arguments to pass to Text constructor

        Returns
        -------
        matplotlib.patches.Rectangle
        """
        from matplotlib.patches import Rectangle
        return Rectangle(xy=(self.x, self.y), width=self.width, height=self.height, **kwargs)
class OvalRoi(Roi):
    def __init__(self, x, y, width, height, **kwargs):
        import numpy as np

        super().__init__(**kwargs)
        self.x = x
        self.y = y
        self.width = width
        self.height = height

        mx,my = np.meshgrid(np.arange(self.image_size[1]), np.arange(self.image_size[0]))
        midpoint = (self.x + self.width/2, self.y + self.height/2)
        self.mask = ((mx-midpoint[0])/(self.width/2))**2 + ((my-midpoint[1])/(self.height/2))**2 <= 1
    def get_patch(self, **kwargs):
        """
        Parameters
        ----------
        **kwargs: extra keyword arguments to pass to Text constructor

        Returns
        -------
        matplotlib.patches.Ellipse
        """
        from matplotlib.patches import Ellipse
        return Ellipse(xy=(self.x+self.width/2, self.y+self.height/2), width=self.width, height=self.height, **kwargs)

class LineRoi(Roi):
    def __init__(self, x1, y1, x2, y2, **kwargs):
        super().__init__(**kwargs)
        self.p1 = (x1, y1)
        self.p2 = (x2, y2)
class ArrowRoi(LineRoi):
    def __init__(self, x1, y1, x2, y2, double_headed, outline, style, head_size, **kwargs):
        super().__init__(x1, y1, x2, y2, **kwargs)
        self.double_headed = double_headed
        self.outline = outline
        self.style = style
        self.head_size = head_size

class EllipseRoi(Roi):
    def __init__(self, x1, y1, x2, y2, aspect_ratio, **kwargs):
        super().__init__(**kwargs)

class PointRoi(Roi):
    def __init__(self, points, **kwargs):
        import numpy as np
        super().__init__(**kwargs)

        self.points = points
        self.mask = np.zeros(self.image_size, dtype='bool')
        for p in points:
            self.mask[p[1], p[0]] = True
        self.x = min([p[1] for p in self.points])
        self.y = min([p[0] for p in self.points])
        self.width = max([p[1] for p in self.points]) - self.x
        self.height = max([p[0] for p in self.points]) - self.y
    def get_patch(self, **kwargs):
        """
        Parameters
        ----------
        **kwargs: extra keyword arguments to pass to Text constructor

        Returns
        -------
        matplotlib.patches.Polygon
        """
        from matplotlib.patches import Polygon
        return Polygon(xy = self.points, **kwargs)
class PolygonRoi(PointRoi):
    def __init__(self, points, **kwargs):
        super().__init__(points, **kwargs)
class FreeRoi(PointRoi):
    def __init__(self, points, **kwargs):
        import numpy as np
        from PIL import Image, ImageDraw
        super().__init__(points, **kwargs)
        img = Image.new('L', (self.image_size[1], self.image_size[0]), 0)
        ImageDraw.Draw(img).polygon(points, outline=1, fill=1)
        self.mask = np.array(img, dtype='bool')
class TracedRoi(PointRoi):
    def __init__(self, points, **kwargs):
        import numpy as np
        from PIL import Image, ImageDraw
        super().__init__(points, **kwargs)
        img = Image.new('L', (self.image_size[1], self.image_size[0]), 0)
        ImageDraw.Draw(img).polygon(points, outline=1, fill=1)
        self.mask = np.array(img, dtype='bool')
class PolylineRoi(PointRoi):
    def __init__(self, points, **kwargs):
        super().__init__(points, **kwargs)
class FreelineRoi(PointRoi):
    def __init__(self, points, **kwargs):
        super().__init__(points, **kwargs)
class AngleRoi(PointRoi):
    def __init__(self, points, **kwargs):
        super().__init__(points, **kwargs)
