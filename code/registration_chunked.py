"""
Register each trial in a set by aligning the average projections. Uses DFT registration algorithm from SIMA
"""
import os, datetime, time, re, sys, logging
import h5py
import numpy as np
import multiprocessing
from functools import partial
from sima.motion.dftreg import _register_frame
from scipy.ndimage.interpolation import shift

def register(data, target, n_processes=4):
    """
    Register the trials in data to the target
    """

    map_function = partial(_register_frame, mean_img=target,
                           upsample_factor=4)

    pool = multiprocessing.Pool(n_processes)
    results = pool.map(map_function, data)
    pool.close()

    dy = [r[0] for r in results]
    dx = [r[1] for r in results]

    return dy,dx

def shift(dy, dx, data):
    """
    Shift data
    """
    posX = dx[dx>0]
    negX = dx[dx<0]
    posY = dy[dy>0]
    negY = dy[dy<0]

    max_pos = [0,0]
    max_neg = [0,0]

    if len(posY) > 0:
        max_pos[0] = np.ceil(np.max(np.abs(posY)))
    if len(posX) > 0:
        max_pos[1] = np.ceil(np.max(np.abs(posX)))
    if len(negY) > 0:
        max_neg[0] = np.ceil(np.max(np.abs(negY)))
    if len(negX) > 0:
        max_neg[1] = np.ceil(np.max(np.abs(negX)))

    max_pos = [int(np.round(x)) for x in max_pos]
    max_neg = [int(np.round(x)) for x in max_neg]

    new_size = np.array(data.shape)
    new_size[2] += max_pos[0] + max_neg[0]
    new_size[3] += max_pos[1] + max_neg[1]
    embed = np.zeros(new_size)
    starty = max_neg[0]
    startx = max_neg[1]
    for trial in range(data.shape[0]):
        shifty = starty + dy[trial]
        shiftx = startx + dx[trial]
        embed[trial,:,shifty:data.shape[2]+shifty, shiftx:data.shape[3]+shiftx] = data[trial]

    return embed

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('exptdir', help='Name of experiment within datadir')
    parser.add_argument('--max_mem', default='512M', help='Maximum memory to use for simultaneously loaded data files. Understands "K", "M", "G" for kilo, mega, giga. Default is "512M"')
    parser.add_argument('-d', '--datadir', help='Where to look for data. Overrides $DATADIR')
    parser.add_argument('--hfiledir', help='Where to save the hdf5 file. Overrides $HFILEDIR')
    parser.add_argument('-o', '--outputdir', help='Where to store files. Overrides $OUTPUTDIR')
    parser.add_argument('-l', '--log', default='./registration_exptdir_yymmdd_hhmmss.log', help='Log file')
    args = parser.parse_args()

    exptdir = args.exptdir

    metric_prefixes = {'K': int(2**10),
                       'M': int(2**20),
                       'G': int(2**30)}
    m = re.match('([0-9]+)([A-Z]*)', args.max_mem)
    max_mem = int(m.group(1)) * metric_prefixes[m.group(2)]

    if ('DATADIR' in os.environ) and (os.environ['DATADIR'] != ''):
        datadir = os.environ['DATADIR']
    elif not(args.datadir is None):
        datadir = args.datadir
    else:
        raise NameError("datadir is not defined. --datadir option or $DATADIR environment variable must be set.")
    if ('HFILEDIR' in os.environ) and (os.environ['HFILEDIR'] != ''):
        hfiledir = os.environ['HFILEDIR']
    elif not(args.hfiledir is None):
        hfiledir = args.hfiledir
    else:
        raise NameError("hfiledir is not defined. --hfiledir option or $HFILEDIR environment variable must be set.")
    if ('OUTPUTDIR' in os.environ) and (os.environ['OUTPUTDIR'] != ''):
        outputdir = os.environ['OUTPUTDIR']
    elif not(args.outputdir is None):
        outputdir = args.outputdir
    else:
        raise NameError("outputdir is not defined. --outputdir option or $OUTPUTDIR environment variable must be set.")

    exptname = os.path.split(exptdir)[-1]
    if args.log == './registration_exptdir_yymmdd_hhmmss.log':
        logfile = os.path.join( os.path.dirname(os.path.realpath(__file__)),'logfiles/registration_'+exptname+'_'+datetime.datetime.now().strftime('%y%m%d_%H%M%S')+'.log')
    else:
        logfile = args.log

    logger = logging.getLogger()
    logger.setLevel(0)
    formatter = logging.Formatter('{0} -- %(message)s'.format(exptname))
    shandler = logging.StreamHandler()
    shandler.setLevel(logging.DEBUG)
    shandler.setFormatter(formatter)
    fhandler = logging.FileHandler(logfile)
    fhandler.setLevel(logging.INFO)
    fhandler.setFormatter(formatter)
    logger.addHandler(shandler)
    logger.addHandler(fhandler)
    logger.info(exptname)

    exptdate = re.match('.*/([0-9]{6})', exptdir).group(1)
    exptdate = datetime.date(int('20'+exptdate[0:2]), int(exptdate[2:4]), int(exptdate[4:6]))

    if __debug__:
        hfile_path = os.path.join(hfiledir, exptdir+'.hdf5')#_testing')
        import pdb
    else:
        hfile_path = os.path.join(hfiledir, exptdir+'.hdf5')

    with h5py.File(hfile_path, 'a') as hfile:
        for fov in hfile.values():
            for set_ in fov.values():
                if __debug__:
                    if set_.name != '/fov3/odors_rois_p25_t4_FOV_0':
                        continue
                t_start = time.time()

                target = set_['data/img_data'][0].mean(axis=0)

                trialinfo = set_.attrs
                filesz = 8*trialinfo['fov_x_res']*trialinfo['fov_y_res']*trialinfo['frameinfo_nframes']
                nfiles = int(max_mem / filesz)
                inds = list(range(0, set_['data/img_data'].shape[0], nfiles))
                indexlist = []
                indices = list(range(set_['data/img_data'].shape[0]))
                for n,i in enumerate(inds[:-1]):
                    indexlist.append(indices[inds[n]:inds[n+1]])
                indexlist.append(indices[inds[-1]:])

                chunkind = 0
                dy = np.zeros(set_['data/img_data'].shape[0])
                dx = np.zeros(set_['data/img_data'].shape[0])
                for (chunknum, inds) in enumerate(indexlist):
                    try:
                        chunk = set_['data/img_data'][inds].mean(axis=1)
                        (chunk_dy, chunk_dx) = register(chunk, target)
                        dy[chunkind:chunkind+len(inds)] = chunk_dy
                        dx[chunkind:chunkind+len(inds)] = chunk_dx
                    except Exception as err:
                        info_str = '{0}/{1}/{2} >> Error while processing chunk {3}\n'.format(exptdir, fov.name, set_.name, chunknum)
                        logger.warning(info_str, exc_info=True)
                    finally:
                        chunkind += len(inds)

                embed = shift(dy,dx,set_['data/img_data'][...])
                if __debug__: pdb.set_trace()

                if 'data/registered_data' in set_:
                    del set_['data/registered_data']
                reg_dataset = set_.create_dataset('data/registered_data',
                                                  data=embed,
                                                  compression='lzf',
                                                  shuffle=True,
                                                  fletcher32=True)
                reg_dataset.dims[0].label = 'Trial'
                reg_dataset.dims[1].label = 'Frame'
                reg_dataset.dims[2].label = 'y'
                reg_dataset.dims[3].label = 'x'
                t_elapsed = time.time() - t_start
                logger.debug('{0}/{1} took {2} seconds'.format(fov.name, set_.name, t_elapsed))
