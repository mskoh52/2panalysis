"""
Goes through sets and processes spotmaps
"""
import os, re, glob, pickle, datetime
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import seaborn as sns
import numpy as np
from scipy.stats import t as tdist
import pandas as pd
import csv
from PIL import Image
import readIJrois as rr
from image_anal_helpers import *
import tifffile
import h5py

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('exptdir', help='Name of experiment within datadir')
    parser.add_argument('-d', '--datadir', help='Where to look for data. Overrides $DATADIR')
    parser.add_argument('-o', '--outputdir', help='Where to store files. Overrides $OUTPUTDIR')
    parser.add_argument('-l', '--log', default='./analyze_rois.log', help='Log file')
    parser.add_argument('--gridrois', action='store_true', help='Use a 10x10 grid to analyze images instead of hand-drawn ROIs')
    parser.add_argument('--savefigs', action='store_true', help='Save figures in outputdir')
    parser.add_argument('--use-raw-zscore', action='store_true', help='Use raw data to zscore instead of dF/F')
    parser.add_argument('--plot-selection', default='dff')
    args = parser.parse_args()
    roi_key = 'gridroi_data' if args.gridrois else 'roi_data'
    exptdir = args.exptdir
    use_raw_zscore = args.use_raw_zscore
    plot_selection = args.plot_selection
    if ('DATADIR' in os.environ) and (os.environ['DATADIR'] != ''):
        datadir = os.environ['DATADIR']
    elif not(args.datadir is None):
        datadir = args.datadir
    else:
        raise NameError("datadir is not defined. --datadir option or $DATADIR environment variable must be set.")
    if ('OUTPUTDIR' in os.environ) and (os.environ['OUTPUTDIR'] != ''):
        outputdir = os.environ['OUTPUTDIR']
    elif not(args.outputdir is None):
        outputdir = args.outputdir
    else:
        raise NameError("outputdir is not defined. --outputdir option or $OUTPUTDIR environment variable must be set.")
else:
    raise Exception("run it from command line")

sns.set_style('dark')

with open(args.log, 'a') as logfile:
    logfile.write('--{0}: {1}--\n'.format(datetime.datetime.now(), exptdir))

def make_maps(roi_data, spots, nx, ny, baseline, stimulus, normalization='dff', baseline_sd = None):
    """ Make spotmaps

    Returns:
    spotmap
    spotmap_sd
    spotmap_count

    Arguments:
    roi_data -- the data
    spots -- list of spots for each trial
    nx -- # of x spots
    ny -- # of y spots
    baseline -- which frames to use as baseline [)
    stimulus -- which frames to use as stimulus [)

    Keyword arguments:
    normalization -- {['dff'], 'zscore', 'meansub', 'none'}
    baseline_sd -- standard devation in baseline period, must be provided if normalization='zscore'
    """
    spotmap = np.zeros((roi_data.shape[0], ny, nx))
    spotmap_sd = np.zeros((roi_data.shape[0], ny, nx))
    count = np.zeros((ny, nx))

    baseline_mean = np.mean(roi_data[:,:,baseline[0]:baseline[1]], axis=2)
    stimulus_mean = np.mean(roi_data[:,:,stimulus[0]:stimulus[1]], axis=2)
    meansub = roi_data - baseline_mean
    if normalization=='zscore':
        if baseline_sd is None: raise ValueError('baseline_sd must be provided when using zscore normalization')
        roi_data = meansub/baseline_sd
    elif normalization=='dff':
        roi_data = meansub/baseline_mean
    elif normalization=='meansub':
        roi_data = meansub
    elif normalization=='none':
        roi_data = meansub
    else:
        raise ValueError('Unknown value assigned to normalization')
    response = np.mean(roi_data[:,:,stimulus[0]:stimulus[1]], axis=2)

    for t in range(int(nx*ny)):
        inds = np.unravel_index(t, spotmap.shape[1:3])
        spotmap[:, inds[0], inds[1]] = np.mean(response[:,spots==t], axis=1)
        spotmap_sd[:, inds[0], inds[1]] = np.std(response[:,spots==t], axis=1, ddof=1)
        count[inds[0], inds[1]] = np.count_nonzero(spots==t)
    return spotmap, spotmap_sd, count

def contig_size(condition):
    if not(any(condition)): return np.array([0])
    d = np.diff(condition, axis=0)
    idx = d.nonzero()[0]
    if condition[0]:
        idx = np.r_[-1, idx]
    if condition[-1]:
        idx = np.r_[idx, condition.shape[0]-1]
    return np.squeeze(np.diff(idx.reshape(-1,2)))

with h5py.File(os.path.join(datadir, exptdir+'_analysis.hdf5')) as hfile:
    for (fovnum, (fovname, fov)) in enumerate(hfile.items()):
        for (setnum, (setname, set_)) in enumerate(fov.items()):
            try:
                if (setname.find('rois') >= 0) or setname.find('glomstim') >= 0 or setname.find('odors') >= 0: continue

                print('{6} -- {0} ({1:d}/{2:d}): {3} ({4:d}/{5:d})'.format(fovname, fovnum+1, len(hfile.values()), setname, setnum+1, len(fov.items()), exptdir))

                roi_data = set_[roi_key][...]
                spots = set_['meta/Spots'][...]
                odors = set_['meta/Odors'][...]

                trialinfo = set_.attrs
                if not(trialinfo['frameinfo_use_light']): continue

                skipframes = [trialinfo['light_pre_light']]
                if trialinfo['frameinfo_use_light']:
                    baseline = (max(0,trialinfo['light_pre_light']-5), trialinfo['light_pre_light'])
                    stimulus = (trialinfo['light_pre_light'], trialinfo['light_pre_light']+trialinfo['light_on_light']+3)
                    post = (trialinfo['light_pre_light']+trialinfo['light_on_light'], trialinfo['frameinfo_nframes'])
                elif trialinfo['frameinfo_use_odor']:
                    baseline = (0, trialinfo['odor_pre_odor'])
                    stimulus = (trialinfo['odor_pre_odor'], trialinfo['odor_pre_odor']+trialinfo['odor_on_odor'])
                    post = (trialinfo['odor_pre_odor']+trialinfo['odor_on_odor'], trialinfo['frameinfo_nframes'])
                else:
                    baseline = (0, trialinfo['frameinfo_nframes'])
                    stimulus = (0, trialinfo['frameinfo_nframes'])
                    post = (0, trialinfo['frameinfo_nframes'])

                roi_data = np.delete(roi_data, skipframes, axis=2)
                baseline_mean = np.mean(roi_data[:,:,baseline[0]:baseline[1]], axis=2)
                baseline_sd = np.std(roi_data[:,:,baseline[0]:baseline[1]], axis=2, ddof=1)
                stimulus_mean = np.mean(roi_data[:,:,stimulus[0]:stimulus[1]], axis=2)

                zscore = (roi_data - baseline_mean[:,:,None]) / baseline_sd[:,:,None]
                dff = (roi_data - baseline_mean[:,:,None]) / baseline_mean[:,:,None]

                baseline_sd_dff = np.std(dff[:,:,baseline[0]:baseline[1]], axis=0, ddof=1)
                2score_dff = dff / baseline_sd_dff[:,:,None]

                # Make map
                spotsz = trialinfo['stimcluster_Spot size (px)']
                mapsz_x = trialinfo['fos_width']
                mapsz_y = trialinfo['fos_height']
                nx = np.round(mapsz_x / spotsz)
                ny = np.round(mapsz_y / spotsz)

                if trialinfo['frameinfo_stim_type'] == 'Spotmap':
                    spotmap, spotmap_sd, spotmap_count = make_maps(roi_data, spots, nx, ny, baseline, stimulus, normalization='none')
                    if args.gridrois:
                        spotmap_key = 'grid_spotmap'
                    else:
                        spotmap_key = 'spotmap'
                    if spotmap_key in set_:
                        del set_[spotmap_key]
                        del set_[spotmap_key+'_sd']
                        del set_[spotmap_key+'_count']
                    set_.create_dataset(spotmap_key, data=spotmap)
                    set_.create_dataset(spotmap_key+'_sd', data=spotmap_sd)
                    set_.create_dataset(spotmap_key+'_count', data=spotmap_count)

                # Delete responded key if it exists from an old version
                if args.gridrois:
                    responded_key = 'grid_responded'
                else:
                    responded_key = 'responded'
                if responded_key in set_:
                    del set_[responded_key]

                if args.savefigs:
                    # Find rois that responded
                    zs_threshold = 2
                    if use_raw_zscore:
                        condition = zscore > zs_threshold
                    else:
                        condition = zscore_dff > zs_threshold
                    sz = [[contig_size(c) for c in cond] for cond in condition.transpose(1,2,0)] # indexed as sz[roi][trial]
                    roiszs = [np.hstack(roi) for roi in sz]
                    ##### Choose min_contig_size such that:
                    ##### .05 > z(zs_threshold) ** min_contig_size * (roi_data.shape[0] - min_contig_size + 1) * roi_data.shape[2]
                    min_contig_size = 3
                    fraction_passing = np.array([np.count_nonzero(x >= min_contig_size)/x.shape[0] for x in roiszs])

                    responded = fraction_passing > 0.05
                    goodmaps = spotmap[responded,:,:]

                    # Make average image
                    avg = tifffile.imread(os.path.join(outputdir, exptdir, setname[1:].replace('/','_').replace('_FOV_0', '/FOV_0'), 'all_avg.tif'))
                    if len(avg.shape) == 2: avg = avg.reshape((-1, avg.shape[0], avg.shape[1]))

                    # Load rois
                    if args.gridrois:
                        rois = pickle.loads(set_['gridrois'][...].tostring())
                    else:
                        rois = pickle.loads(set_['rois'][...].tostring())

                    # Plot maps that responded
                    for n,mp in zip(responded.nonzero()[0],goodmaps):
                        roi = rois[n]

                        tstat = spotmap[n]/(spotmap_sd[n] / np.sqrt(spotmap_count))
                        p = tdist.cdf(tstat, spotmap_count-1)
                        wheresig = np.transpose((p > .95).transpose().nonzero())

                        fig = plt.figure(figsize=(19.2,9.8),dpi=100)
                        ax0 = plt.subplot2grid( (2,2), (0, 0) )
                        m_im = ax0.imshow(mp, interpolation='nearest', cmap=plt.cm.coolwarm, vmin = -mp.max(), vmax = mp.max())
                        plt.colorbar(m_im, ax=ax0)
                        for s in wheresig:
                            ax0.annotate('*', xy = s)

                        ax1 = plt.subplot2grid( (2,2), (0, 1) )
                        p_im = ax1.imshow(np.mean(avg, axis=0), interpolation='nearest', cmap=plt.cm.gray)
                        e = Ellipse(xy=(roi.x, roi.y), width=roi.width, height=roi.height, linewidth=0)
                        ax1.add_artist(e)
                        e.set_alpha(0.7)
                        e.set_facecolor((1,0,0))
                        plt.title('ROI {0}'.format(n))

                        if plot_selection == 'zscore_dff':
                            plt_sel = zscore_dff
                        elif plot_selection == 'zscore':
                            plt_sel = zscore
                        else:
                            plt_sel = dff
                        with sns.axes_style('darkgrid'):
                            ax2 = plt.subplot2grid( (2,2), (1, 0), colspan=2)
                            ax2.plot(plt_sel[:,n,:])
                            ax2.hold('on')
                            ax2.plot(np.mean(plt_sel[:,n,:], axis=1), linewidth=3, color='k')
                        if args.gridrois:
                            plt.savefig(os.path.join(outputdir, exptdir, setname[1:].replace('/','_').replace('_FOV_0', '/FOV_0'), 'gridroi_{0}_'.format(n) + plot_selection + '.png'), dpi=100)
                        else:
                            plt.savefig(os.path.join(outputdir, exptdir, setname[1:].replace('/','_').replace('_FOV_0', '/FOV_0'), 'roi_{0}_'.format(n) + plot_selection + '.png'), dpi=100)
                        plt.close()
            except Exception as err:
                info_str = '{0}/{1}/{2} >> Error while processing set'.format(exptdir, fovname, setname)
                print(info_str)
                with open(args.log, 'a') as logfile:
                    error_handler(err, info_str, logfile)
