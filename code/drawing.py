import numpy as np

def draw_line(p1, p2, antialias=False):
    """Draw lines from p1 to p2 using Bresenham algorithm"""
    #TODO implement antialiasing http://en.wikipedia.org/wiki/Xiaolin_Wu%27s_line_algorithm

def draw_quad(p1, p2, cp):
    """Draw a quadratic curve between p1 and p2 with control point cp"""
    return None

def draw_cubic(p1, p2, cp1, cp2):
    """Draw a cubic curve between p1 and p2 with control points cp1 and cp2"""
    return None

def fill_outline(outline_mask):
    """Takes outline_mask and fills interior points"""

    for row in outline_mask:
        intersections = np.argwhere(row).flatten()
        if len(intersections) > 0:
            crossing_points = intersections[np.append(np.nan, np.diff(intersections, axis=0))!=1]
            if len(crossing_points) > 1:
                row[crossing_points[0]:crossing_points[1]] = True

    return outline_mask
