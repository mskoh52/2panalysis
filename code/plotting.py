import matplotlib
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.colors import Normalize
import warnings
import pickle
import numpy as np
import readIJrois as rr
import re

# change index vs. # of glomeruli, each line is a different rois

class SettingsDict(dict):
    """
    Dictionary with key/value validation
    """
    # Inspired by matplotlib.RcParams

    def __init__(self, parent, *args, **kwargs):
        self.parent = parent
        # Define a list of valid keys and values. First item is default
        self._validdict = { 'IncludePostLightFrame': (0,1),
                            'Primary stimulus': ('light', 'odor'),
                            'ROI set': ('rois', 'gridrois'),
                            'NormalizeRatioImage': ('baseline', 'none', 'boost'),
                            'UseRegisteredData': (0,1) }

        # Start off with list of default values. Will get overwritten if valid key/value is given in init args
        validated = [ (k, v[0]) for (k,v) in self._validdict.items() ]
        for key,val in dict(*args, **kwargs).items():
            if (key in self._validdict) and (val in self._validdict[key]):
                validated.append( (key,val) )

        super().__init__(validated)

    def __setitem__(self, key, val):
        if (key in self._validdict) and (val in self._validdict[key]):
            super().__setitem__(key,val)
        else:
            raise ValueError('Invalid key or value {0}' (key, val))

    # Force use of our special __setitem__ for update method
    def update(self, *args, **kwargs):
        for key,val in dict(*args, **kwargs).items():
            self[key] = val

    def __repr__(self):
        if len(self) == 0:
            s = '{}'
        else:
            s = '{'
            for k,v in sorted(self.items()):
                s += '{0}: {1}\n'.format(k, v)
            s = s[:-1]
            s += '}'
        return s

    def keys(self):
        return sorted(self.keys())

    def values(self):
        return [self[k] for k in self.keys()]

class Experiment:
    def __init__(self, h5set):
        # Load data from the h5set
        self.name = h5set.name
        self.filename = h5set.file.filename
        self.h5set = h5set
        self.trialinfo = dict(h5set.attrs)
        try:
            self.fovimg = h5set['data/registered_data'][...].mean(axis=0).mean(axis=0)
        except KeyError:
            self.fovimg = h5set['data/img_data'][...].mean(axis=0).mean(axis=0)
        self.spots = h5set['meta/Spots'][...]
        self.odors = h5set['meta/Odors'][...]
        self.glomstimuli = h5set['meta/GlomStimuli'][...]
        try:
            self.rois = pickle.loads(h5set['meta/rois'][...].tostring())
            self._roi_data = h5set['data/roi_data'][...]
        except:
            warnings.warn('Could not find rois in hdf5 file')
        try:
            self.gridrois = pickle.loads(h5set['meta/gridrois'][...].tostring())
            self._gridroi_data = h5set['data/gridroi_data'][...]
        except:
            warnings.warn('Could not find gridrois in hdf5 file')
        self._odor_baseline_period = (0,0)
        self._light_baseline_period = (0,0)
        self._odor_response_period = (0,0)
        self._light_response_period = (0,0)

        # Initialize settings with default values
        self.settings = SettingsDict(self)

        # Set up plotting
        self._rc = matplotlib.rc_params()

        # Attempt to figure out what this experiment run was for and set the baseline period,
        # stimulus period, trial types, etc. accordingly
        #
        # First order of business: was there light? was there odor?
        self.trialtype = 0
        if self.trialinfo['frameinfo_use_odor']:
            self.trialtype += 1
        if self.trialinfo['frameinfo_use_light']:
            self.trialtype += 2
            #if self.trialinfo['frameinfo_stim_type'] == 'Spotmap':
            #elif self.trialinfo['frameinfo_stim_type'] == 'Glomstim':
            #elif self.trialinfo['frameinfo_stim_type'] == 'Gridstim':
            #elif self.trialinfo['frameinfo_stim_type'] == 'Blank':
            #else # Should never happen unless something weird is happening in labview code or we forgot to handle a new stim type
            #    raise ValueError('Could not determine stim type, even though light stim was supposed to be on')

        if self.trialtype == 0: # No odor or light
            self.odor_baseline_period = (0,0)
            self.odor_response_period = (0,0)
            self.light_response_period = (0,0)
        elif self.trialtype == 1: # Odor only
            self.settings['Primary stimulus'] = 'odor' #override default
            self.odor_baseline_period = ( self.trialinfo['odor_pre_odor']-10, self.trialinfo['odor_pre_odor'] )
            self.odor_response_period = ( self.trialinfo['odor_pre_odor'], self.trialinfo['odor_pre_odor']+self.trialinfo['odor_on_odor'] )
            self.light_baseline_period = (0,0)
            self.light_response_period = (0,0)
        elif self.trialtype == 2: # Light only
            self.odor_baseline_period = (0,0)
            self.odor_response_period = (0,0)
            self.light_baseline_period = ( self.trialinfo['light_pre_light']-10, self.trialinfo['light_pre_light'] )
            self.light_response_period = ( self.trialinfo['light_pre_light'], self.trialinfo['light_pre_light']+self.trialinfo['light_on_light']  )
        elif self.trialtype == 3: # Odor + Light
            self.odor_baseline_period = ( self.trialinfo['odor_pre_odor']-10, self.trialinfo['odor_pre_odor'] )
            self.odor_response_period = ( self.trialinfo['odor_pre_odor'], self.trialinfo['odor_pre_odor']+self.trialinfo['odor_on_odor'] )
            self.light_baseline_period = ( self.trialinfo['light_pre_light']-10, self.trialinfo['light_pre_light'] )
            self.light_response_period = ( self.trialinfo['light_pre_light'], self.trialinfo['light_pre_light']+self.trialinfo['light_on_light'] )

        self.unique_odors = sorted(list(set(self.odors)))

        self.unique_spots = []
        for s in self.spots:
            found = False
            for u in self.unique_spots:
                if np.array_equal(u,s):
                    found = True
                    break
            if not found:
                self.unique_spots.append(s)

        self.unique_conditions = []
        self.condition_count = []
        for o, s in zip(self.odors, self.spots):
            found = False
            for n,u in enumerate(self.unique_conditions):
                if np.array_equal(u[0], o) and np.array_equal(u[1], s):
                    found = True
                    self.condition_count[n] += 1
                    break
            if not found:
                self.unique_conditions.append((o, s))
                self.condition_count.append(1)

    @property
    def ratioimg(self):
        if self.settings['Primary stimulus'] == 'odor':
            baseline_period = self.odor_baseline_period
            response_period = self.odor_response_period
        else:
            baseline_period = self.light_baseline_period
            response_period = self.light_response_period

        # check cached value of settings to see if we need to recalculate
        current_values = [self.settings['Primary stimulus'], self.settings['IncludePostLightFrame'], self.settings['NormalizeRatioImage'], baseline_period, response_period]
        if not hasattr(self, '_ExperimentRun__ratioimg_cache'): # runs on first call
            self.__ratioimg_cache = [[]]*(len(current_values)+1)
        if current_values == self.__ratioimg_cache[:-1]: # if no settings changed, return the cached ratio image
            return self.__ratioimg_cache[-1]
        if ((baseline_period[1] - baseline_period[0]) <= 0) or (response_period[1] - response_period[0] <= 0): return

        img_data = self.h5set['data/img_data']
        if self.settings['IncludePostLightFrame'] == 0:
            img_data = np.delete(img_data, self.trialinfo['light_pre_light'], axis=1)

        baseline = np.nanmean(img_data[:, baseline_period[0]:baseline_period[1], :, :], axis=1)
        response = np.nanmean(img_data[:, response_period[0]:response_period[1], :, :], axis=1)

        if self.settings['NormalizeRatioImage'] == 'baseline':
            ratioimg = (response - baseline)/baseline
        elif self.settings['NormalizeRatioImage'] == 'none':
            ratioimg = (response - baseline)
        elif self.settings['NormalizeRatioImage'] == 'boost':
            ratioimg = (response - baseline)/(baseline+500)
        ratioimg[np.isinf(ratioimg)] = np.nan

        self.__ratioimg_cache[:-1] = current_values
        self.__ratioimg_cache[-1] = ratioimg
        return ratioimg

    @property
    def rc(self):
        return self._rc
    @rc.setter
    def rc(self, rc):
        if not isinstance(rc, matplotlib.RcParams): raise ValueError('Expected a matplotlib.RcParams object')
        self._rc = rc

    @property
    def rccontext(self):
        return matplotlib.rc_context(rc=self.rc)

    @property
    def odor_baseline_period(self):
        return self._odor_baseline_period
    @odor_baseline_period.setter
    def odor_baseline_period(self, new):
        if not (isinstance(new, tuple) and len(new) == 2):
            raise ValueError('Baseline period must be a length two tuple')
        self._odor_baseline_period = ( max(0, new[0]), min(self.trialinfo['frameinfo_nframes'], new[1]) )

    @property
    def light_baseline_period(self):
        return self._light_baseline_period
    @light_baseline_period.setter
    def light_baseline_period(self, new):
        if not (isinstance(new, tuple) and len(new) == 2):
            raise ValueError('Baseline period must be a length two tuple')
        self._light_baseline_period = ( max(0, new[0]), min(self.trialinfo['frameinfo_nframes'], new[1]) )

    @property
    def odor_response_period(self):
        return self._odor_response_period
    @odor_response_period.setter
    def odor_response_period(self, new):
        if not (isinstance(new, tuple) and len(new) == 2):
            raise ValueError('Odor response period must be a length two tuple')
        self._odor_response_period = ( max(0, new[0]), min(self.trialinfo['frameinfo_nframes'], new[1]) )

    @property
    def light_response_period(self):
        return self._light_response_period
    @light_response_period.setter
    def light_response_period(self, new):
        if not (isinstance(new, tuple) and len(new) == 2):
            raise ValueError('Light response period must be a length two tuple')
        self._light_response_period = ( max(0, new[0]), min(self.trialinfo['frameinfo_nframes'], new[1]) )

    @property
    def data(self):
        # Select the correct data
        if self.settings['ROI set'] == 'rois':
            data = self._roi_data
        elif self.settings['ROI set'] == 'gridrois':
            data = self._gridroi_data
        # Drop or don't drop the light frame
        if self.settings['IncludePostLightFrame']:
            return data
        else:
            return np.delete(data, self.trialinfo['light_pre_light'], axis=2)

    @property
    def dff(self):
        data = self.data
        if self.settings['Primary stimulus'] == 'odor':
            baseline_period = self.odor_baseline_period
        else:
            baseline_period = self.light_baseline_period
        return (data - np.nanmean(data[:,:,baseline_period[0]:baseline_period[1]], axis=2, keepdims=True))/np.nanmean(data[:,:,baseline_period[0]:baseline_period[1]], axis=2, keepdims=True)

    @property
    def dff_by_condition(self):
        dff = self.dff
        responses = np.full((len(self.unique_conditions), self.data.shape[0], max(self.condition_count), dff.shape[2]), np.nan)
        for c in range(len(self.unique_conditions)):
            stim = self.unique_conditions[c]
            condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
            this_cond_response = self.dff[:, condition_mask, :]
            responses[c,:,:this_cond_response.shape[1],:] = this_cond_response
        return responses

    def display_rois(self, roi_subset=None, display_roi_numbers=True):
        """
        Draws outlines of rois on fov image
        Parameters
        ----------
        roi_subset : iterable, optional
                     list of rois to plot
        display_roi_numbers : bool, optional
        Returns
        -------
        fig, ax

        """
        if roi_subset is None:
            rois = self.rois if (self.settings['ROI set'] == 'rois') else self.gridrois
        else:
            if self.settings['ROI set'] == 'rois':
                rois = [self.rois[r] for r in roi_subset]
            else:
                rois = [self.gridrois[r] for r in roi_subset]

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(self.fovimg, vmin = np.percentile(self.fovimg, .25), vmax = np.percentile(self.fovimg, 99.75), cmap = plt.cm.gray, interpolation='nearest')

        ells = []
        labels = []
        for n,r in enumerate(rois):
            try:
                for subroi in r.subrois: ax.add_artist( subroi.get_patch(linewidth=1, facecolor='none', edgecolor='y') )
            except AttributeError:
                ax.add_artist( r.get_patch(linewidth=1, facecolor='none', edgecolor='y') )
            if display_roi_numbers:
                ax.add_artist(r.get_label(str(n), fontsize='10', color='darkorange', weight='normal', family='monospace', horizontalalignment='center', verticalalignment='center'))

        ax.set_xticks([])
        ax.set_yticks([])
        return fig, ax

    def display_roi_responses(self, condition=None, cmap=plt.cm.CMRmap, normalization='all', subplotshape=None, roi_subset=None, stim_rois=None, print_roi_numbers=True, print_borders=True, use_alpha=True):
        """
        Show the ROIs colored by their response

        Parameters
        ----------
        condition : int or tuple
            Index (indices) into self.unique_conditions
        cmap : matplotlib.colors.LinearSegmentedColormap
            Colormap to use for plotting responses
        normalization : {'all', 'condition', matplotlib.colors.Normalize instance, list of matplotlib.colors.Normalize instances}
            Determines how to scale the colormap. If norm == 'all', the colorbar
            is scaled between the min and max of the responses for all conditions.
            If norm == 'condition', the min and max for the condition being shown
            is used. Otherwise, specify a Normalize instance to be used.
        subplotshape: list or tuple of length 2
            Number of rows and columns of axes to create
        roi_subset: list or tuple
            Only display certain rois
        stim_rois: list of rois
            When provided, will draw dashed blue circles where light stimulus was
        print_roi_numbers: bool
            Print little numbers on top of the rois (default True)
        print_borders: bool
            Print solid borders around rois (default True)

        Returns
        -------
        fig, ax, responses
        """
        from mpl_toolkits.axes_grid1 import ImageGrid

        if condition is None:
            condition = tuple(range(len(self.unique_conditions)))
        elif type(condition) is int:
            condition = (condition,)

        if self.settings['Primary stimulus'] == 'light':
            response_period = self.light_response_period
        elif self.settings['Primary stimulus'] == 'odor':
            response_period = self.odor_response_period

        rois = self.rois if (self.settings['ROI set'] == 'rois') else self.gridrois
        if roi_subset is not None:
            rois = [rois[x] for x in roi_subset]
        else:
            roi_subset = list(range(len(rois)))

        responses = np.zeros( (len(self.unique_conditions), len(rois)) )
        for c in range(len(self.unique_conditions)):
            stim = self.unique_conditions[c]
            condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
            responses[c,:] = np.nanmean(self.dff[:, condition_mask, response_period[0]:response_period[1]], axis=2).mean(axis=1)[roi_subset]

        if subplotshape is None:
            subplotshape = guess_subplotshape(len(condition))
        fig = plt.figure()
        if normalization == 'condition':
            ax = ImageGrid(fig, 111,
                           nrows_ncols = subplotshape,
                           axes_pad = (.5, .25),
                           cbar_mode = 'each',
                           cbar_pad = 0.05)
        elif isinstance(normalization, list):
            if all([isinstance(n, matplotlib.colors.Normalize) for n in normalization]):
                ax = ImageGrid(fig, 111,
                               nrows_ncols = subplotshape,
                               axes_pad = (.5, .25),
                               cbar_mode = 'each',
                               cbar_pad = 0.05)
        elif (normalization == 'all') or (isinstance(normalization, matplotlib.colors.Normalize)):
            ax = ImageGrid(fig, 111,
                           nrows_ncols = subplotshape,
                           axes_pad = (0.1, 0.25),
                           cbar_mode = 'single')
        else:
            raise ValueError('Invalid value passed to normalization')

        for c,cond in enumerate(condition):
            if normalization == 'condition':
                norm = Normalize(np.nanmin(responses[cond,:]), np.nanmax(responses[cond,:]))
                dummy = plt.imshow(responses[cond,np.newaxis], cmap=cmap)
            elif normalization == 'all':
                norm = Normalize(np.nanmin(responses), np.nanmax(responses))
                dummy = plt.imshow(responses, cmap=cmap)
            elif isinstance(normalization, list):
                norm = normalization[c]
                dummy = plt.imshow(responses[cond,np.newaxis], cmap=cmap, norm=norm)
            elif isinstance(normalization, matplotlib.colors.Normalize):
                norm = normalization
                dummy = plt.imshow(responses[cond,np.newaxis], cmap=cmap, norm=norm)

            ax[c].imshow(self.fovimg, vmin = np.percentile(self.fovimg, .25), vmax = 1.5*np.percentile(self.fovimg, 99.75), cmap = plt.cm.gray, interpolation='none')
            ells = []
            bgells = []
            labels = []
            colors = [cmap(norm(r)) for r in responses[cond,:]]
            edgecolors = [cmap(norm(r)) for r in responses[cond,:]]
            if use_alpha:
                alphas = [0.3+0.3*min(1,max(0,norm(r))) for r in responses[cond,:]]
            else:
                alphas = [1]*responses.shape[1]

            for (n, (r, fc, ec, al)) in enumerate(zip(rois, colors, edgecolors, alphas)):
                try:
                    for subroi in r.subrois: ax[c].add_artist( subroi.get_patch(linewidth=1, facecolor=fc, edgecolor='none', alpha=al) )
                except AttributeError:
                    ax[c].add_artist( r.get_patch(linewidth=1, facecolor=fc, edgecolor='none', alpha=al) )
                if print_borders:
                    try:
                        for subroi in r.subrois: ax[c].add_artist( subroi.get_patch(linewidth=1, facecolor='none', edgecolor=ec) )
                    except AttributeError:
                        ax[c].add_artist( r.get_patch(linewidth=1, facecolor='none', edgecolor=ec) )
                if print_roi_numbers:
                    label =r.get_label(str(n), fontsize='10', color='darkorange', weight='normal', family='monospace', horizontalalignment='center', verticalalignment='center')
                    x = label._x
                    y = label._y
                    label._y = x
                    label._x = y
                    ax[c].add_artist( label )

            if stim_rois is not None:
                for r in [stim_rois[x] for x in self.unique_conditions[cond][1]]:
                    try:
                        for subroi in r.subrois: ax[c].add_artist( subroi.get_patch(linewidth=2, facecolor='none', edgecolor='cyan', linestyle='dotted') )
                    except AttributeError:
                        ax[c].add_artist( r.get_patch(linewidth=2, facecolor='none', edgecolor='cyan', linestyle='dotted') )

            if self.trialtype == 1:
                titlestr = 'Odor {0}'.format(self.unique_conditions[cond][0])
            elif self.trialtype == 2:
                titlestr = 'Stim {0}'.format(self.unique_conditions[cond][1])
            elif self.trialtype == 3:
                titlestr = 'Odor {0}, Stim {1}'.format(self.unique_conditions[cond][0], self.unique_conditions[cond][1])
            titlestr = re.sub('\[([0-9]+)\]', '\g<1>', titlestr)
            ax[c].set_title(titlestr, fontsize=16)
            ax[c].set_xticks([])
            ax[c].set_yticks([])
            cb = ax.cbar_axes[c].colorbar(dummy)
            cb.solids.set_edgecolor("face")
            for t in cb.ax.get_yticklabels(): t.set_fontsize(14) # TODO figure out how to set font
            from matplotlib.ticker import FormatStrFormatter
            ax.cbar_axes[c].set_aspect('auto')
            ax.cbar_axes[c].set_yticks( np.linspace(norm.vmin, norm.vmax, 5) )
            ax.cbar_axes[c].yaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        return (fig, ax, responses)

    def display_lvroi_responses(lvroi_file, data, cmap=plt.cm.CMRmap, exclude_lvrois=None):
        import struct, os
        import readIJrois as rr
        # load image from lvroi file
        with open(lvroi_file, 'rb') as f: bytez = f.read()
        imgbytez = bytez[bytez.find(b'IMAGEDATA')+9:]
        try:
            from PIL import Image
            from io import BytesIO
            img = np.asarray(Image.open(BytesIO(imgbytez)))
            sz = img.shape
        except OSError:
            sz = struct.unpack('>2I',imgbytez[0:8])
            img = np.array(struct.unpack('>{0}B'.format(sz[0]*sz[1]), imgbytez[8:]))
            img.shape = sz

        rd = rr.RoiDecoder(sz)
        stimrois = rd.fromfile(lvroi_file, fileformat='labview')

        if exclude_lvrois is not None:
            for x in exclude_lvrois:
                del stimrois[x]
                data = np.delete(data,x)

        norm = matplotlib.colors.Normalize(np.nanmin(data), np.nanmax(data))
        dummy = plt.imshow(np.atleast_2d(data), cmap=cmap)

        fig, ax = plt.subplots(1,1)
        ax.imshow(img,
                vmin = np.percentile(img, .25),
                vmax = np.percentile(img, 99.75),
                cmap = plt.cm.gray, interpolation='none')
        ells = []
        textpos = []
        for n,r in enumerate(stimrois):
            try:
                for subroi in r.subrois: ax.add_artist( subroi.get_patch(linewidth=1, facecolor=cmap(data[n]), edgecolor=None, alpha=.5))
            except AttributeError:
                ax.add_artist( r.get_patch(linewidth=1, facecolor=cmap(data[n]), edgecolor='y', alpha=.5))

        ax.set_xticks([])
        ax.set_yticks([])
        fig.colorbar(dummy)
        return fig, ax, stimrois

    def plot_roi_traces(self, roi_selection=None, subplotshape=None, plot_filter=None ,average=False):
        from matplotlib.lines import Line2D
        if expt.settings['ROI set'] == 'rois':
            rois = self.rois
        else:
            rois = self.gridrois

        if roi_selection is None:
            if expt.settings['ROI set'] == 'rois':
                roi_selection = np.arange(len(self.rois))
            else:
                roi_selection = np.arange(len(self.gridrois))

        if subplotshape is None:
            subplotshape = guess_subplotshape(len(roi_selection))

        condition_average = np.zeros( (len(rois), len(self.unique_conditions), self.data.shape[2]) )
        for c in range(len(self.unique_conditions)):
            stim = self.unique_conditions[c]
            condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
            condition_average[:, c, :] = np.nanmean( self.dff[:, np.atleast_1d(np.argwhere(condition_mask).squeeze()), :], axis=1 )

        if plot_filter is not None:
            condition_average = plot_filter(condition_average)
        if average:
            condition_average = np.nanmean(condition_average, axis=1, keepdims=True)

        fig, ax = plt.subplots(*subplotshape, gridspec_kw={'hspace': .25, 'wspace': .2}, squeeze=False)

        for r, roi in enumerate(roi_selection):
            ax[np.unravel_index(r, subplotshape)].plot( condition_average[roi].T )

            if (self.trialtype == 1) or (self.trialtype == 3): # Odor was on
                ax[np.unravel_index(r, subplotshape)].add_line(Line2D((self.trialinfo['odor_pre_odor']-1,)*2, (-10,10), color='y', linestyle=':'))
                ax[np.unravel_index(r, subplotshape)].add_line(Line2D((self.trialinfo['odor_pre_odor']-1+self.trialinfo['odor_on_odor'],)*2, (-10,10), color='y', linestyle=':'))
            if self.trialtype >= 2: # Light was on
                ax[np.unravel_index(r, subplotshape)].add_line(Line2D((self.trialinfo['light_pre_light']-1,)*2, (-10,10), color='b', linestyle=':'))
            titlestr = 'ROI {0}'.format(roi)
            ax[np.unravel_index(r, subplotshape)].set_title(titlestr)
            ax[np.unravel_index(r, subplotshape)].set_xlim(left = 0, right = self.dff.shape[2])
            if r < subplotshape[1]*(subplotshape[0]-1):
                ax[np.unravel_index(r, subplotshape)].set_xticks([])
        return (fig, ax)

    def plot_condition_traces(self, condition_selection=None, subplotshape=None, plot_filter=None, average=False):
        """
        Plot all the rois for given conditions

        Returns
        -------
        (fig, ax, condition_averages)
        """
        from matplotlib.lines import Line2D

        if self.settings['ROI set'] == 'rois':
            rois = self.rois
        else:
            rois = self.gridrois

        if condition_selection is None:
            condition_selection = tuple(range(len(self.unique_conditions)))
        elif type(condition_selection) is int:
            condition_selection = (condition_selection,)

        if subplotshape is None:
            subplotshape = guess_subplotshape(len(condition_selection))

        condition_average = np.zeros( (len(rois), len(self.unique_conditions), self.data.shape[2]) )
        for c in range(len(self.unique_conditions)):
            stim = self.unique_conditions[c]
            condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
            condition_average[:, c, :] = np.nanmean( self.dff[:, np.atleast_1d(np.argwhere(condition_mask).squeeze()), :], axis=1 )

        if plot_filter is not None:
            condition_average = plot_filter(condition_average)
        if average:
            condition_average = np.nanmean(condition_average, axis=0, keepdims=True)

        fig, ax = plt.subplots(*subplotshape, squeeze=False)

        for c,cond in enumerate(condition_selection):
            ax[np.unravel_index(c, subplotshape)].plot( condition_average[:,cond].T )

            if (self.trialtype == 1) or (self.trialtype == 3): # Odor was on
                ax[np.unravel_index(c, subplotshape)].add_line(Line2D((self.trialinfo['odor_pre_odor']-1,)*2, (-10,10), color='y', linestyle=':'))
                ax[np.unravel_index(c, subplotshape)].add_line(Line2D((self.trialinfo['odor_pre_odor']-1+self.trialinfo['odor_on_odor'],)*2, (-10,10), color='y', linestyle=':'))
            if self.trialtype >= 2: # Light was on
                ax[np.unravel_index(c, subplotshape)].add_line(Line2D((self.trialinfo['light_pre_light']-1,)*2, (-10,10), color='b', linestyle=':'))

            if self.trialtype == 1:
                titlestr = 'Odor {0}'.format(self.unique_conditions[cond][0])
            elif self.trialtype == 2:
                titlestr = 'Stim {0}'.format(self.unique_conditions[cond][1])
            elif self.trialtype == 3:
                titlestr = 'Odor {0}, Stim {1}'.format(self.unique_conditions[cond][0], self.unique_conditions[cond][1])
            titlestr = re.sub('\[([0-9]+)\]', '\g<1>', titlestr)
            ax[np.unravel_index(c, subplotshape)].set_title(titlestr, fontsize=14)
            ax[np.unravel_index(c, subplotshape)].set_xlim(left = 0, right = self.dff.shape[2])
            ax[np.unravel_index(c, subplotshape)].tick_params(axis='both', which='major', labelsize=12)
            if c < subplotshape[1]*(subplotshape[0]-1):
                ax[np.unravel_index(c, subplotshape)].set_xticklabels([])
        return (fig, ax, condition_average)

    #TODO: memoization
    def display_ratio_images(self, condition=None, cmap=plt.cm.CMRmap, normalization='all', subplotshape=None, stim_rois=None, mask=None, kernel=None):
        """
        Parameters
        ----------
        condition : int or tuple
            Index (indices) into self.unique_conditions
        cmap : matplotlib.colors.LinearSegmentedColormap
            Colormap to use for plotting responses
        normalization : {'all', 'condition', matplotlib.colors.Normalize instance, list of matplotlib.colors.Normalize instances}
            Determines how to scale the colormap. If norm == 'all', the colorbar
            is scaled between the min and max of the responses for all conditions.
            If norm == 'condition', the min and max for the condition being shown
            is used. Otherwise, specify a Normalize instance to be used.
        subplotshape: list or tuple of length 2
            Number of rows and columns of axes to create
        stim_rois: list of rois
            When provided, will draw dashed blue circles where light stimulus was
        mask: numpy.ndarray bool
            Mask out pixels of the ratio image for display (True ==> mask it out)
        kernel: numpy.ndarray
            Filter the ratio images with the kernel for display

        Returns
        -------
        (fig, ax, responses)
        """
        from mpl_toolkits.axes_grid1 import ImageGrid

        if condition is None:
            condition = tuple(range(len(self.unique_conditions)))
        elif type(condition) is int:
            condition = (condition,)

        if mask is None:
            mask = np.zeros(self.ratioimg[0].shape, dtype='bool')

        responses = np.zeros( (len(self.unique_conditions), self.trialinfo['fov_y_res'], self.trialinfo['fov_x_res']) )
        for c in range(len(self.unique_conditions)):
            stim = self.unique_conditions[c]
            condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
            responses[c, ...] = np.nanmean(self.ratioimg[condition_mask, ...], axis=0)
            responses[c, mask] = np.nan
            if kernel is not None:
                from astropy.convolution import convolve
                responses[c, ...] = convolve(responses[c, ...], kernel)

        if subplotshape is None:
            subplotshape = guess_subplotshape(len(condition))
        fig = plt.figure()
        if normalization == 'condition':
            ax = ImageGrid(fig, 111,
                           nrows_ncols = subplotshape,
                           axes_pad = (.5, .25),
                           cbar_mode = 'each',
                           cbar_pad = 0.05)
        elif isinstance(normalization, list):
            if all([isinstance(n, matplotlib.colors.Normalize) for n in normalization]):
                ax = ImageGrid(fig, 111,
                               nrows_ncols = subplotshape,
                               axes_pad = (.5, .25),
                               cbar_mode = 'each',
                               cbar_pad = 0.05)
        elif (normalization == 'all') or (isinstance(normalization, matplotlib.colors.Normalize)):
            ax = ImageGrid(fig, 111,
                           nrows_ncols = subplotshape,
                           axes_pad = (0.01, 0.25),
                           cbar_mode = 'single')
        else:
            raise ValueError('Invalid value passed to normalization')

        for c,cond in enumerate(condition):
            if normalization == 'condition':
                norm = Normalize(np.nanpercentile(responses[cond,:], .01), np.nanpercentile(responses[cond,:], 99.99))
            elif normalization == 'all':
                norm = Normalize(np.nanpercentile(responses, .01), np.nanpercentile(responses, 99.99))
            elif isinstance(normalization, list):
                norm = normalization[c]
            elif isinstance(normalization, matplotlib.colors.Normalize):
                norm = normalization

            img = ax[c].imshow(responses[cond], cmap=cmap, norm=norm, interpolation='none')

            if stim_rois is not None:
                ells = []
                for r in [stim_rois[x] for x in self.unique_conditions[cond][1]]:
                    try:
                        for subroi in r.subrois: ax.add_artist( subroi.get_patch(linewidth=1, facecolor='none', edgecolor='cyan', linestyle='dotted') )
                    except AttributeError:
                        ax.add_artist( r.get_patch(linewidth=1, facecolor='none', edgecolor=ec) )

            if self.trialtype == 1:
                titlestr = 'Odor {0}'.format(self.unique_conditions[cond][0])
            elif self.trialtype == 2:
                titlestr = 'Stim {0}'.format(self.unique_conditions[cond][1])
            elif self.trialtype == 3:
                titlestr = 'Odor {0}, Stim {1}'.format(self.unique_conditions[cond][0], self.unique_conditions[cond][1])
            ax[c].set_title(titlestr)
            ax[c].set_xticks([])
            ax[c].set_yticks([])
            cb = ax.cbar_axes[c].colorbar(img)
            cb.solids.set_edgecolor("face")
            ax.cbar_axes[c].set_aspect('auto')
            ax.cbar_axes[c].set_yticks( np.linspace(norm.vmin, norm.vmax, 5) )
        return (fig, ax, responses)

    def map_with_traces(spotmap, rdata, spots):
        from matplotlib import gridspec, colors
        nx, ny = spotmap.shape
        grid = gridspec.GridSpec(nx, ny, wspace = 0., hspace = 0.)
        norm = colors.Normalize(vmin = spotmap.min(), vmax = spotmap.max())
        fig = plt.figure()
        for x in range(nx):
            for y in range(ny):
                ax = plt.subplot(grid[x,y])
                ax.set_axis_bgcolor(plt.cm.coolwarm(norm(spotmap[x,y])))
                ax.xaxis.set_ticks_position('none')
                ax.set_xticks([])
                ax.yaxis.set_ticks_position('none')
                ax.set_yticks([])
                for s in ax.spines.values():
                    s.set_visible(False)
                ax.plot(np.mean(rdata[:, spots==np.ravel_multi_index((x,y), (nx, ny))], axis=1), color='k')
        return fig, ax

    def contact_sheet_traces(nrows, ncols, data, error, errormode='traces', figsize=(10.,5.), dpi=100, gridspec_extra_args={}):
        """
        nrows = # of rows
        ncols = # of cols
        data = ( (ax[0] plot args), (ax[1] plot args), ... )
        error = ( (ax[0] plot args), (ax[1] plot args), ... ) for errormode == 'traces'
                ( (x, y1, y2, kwargs for ax[0]), (x, y1, y2, kwargs for ax[2], ... ) for errormode == 'fill'
        errormode = {'traces', 'fill'}
        size = figure size in inches
        dpi = dpi
        gridspec_extra_args = dict of extra kwargs for gridspec aside from nrows and ncols
        """
        from matplotlib.gridspec import GridSpec
        fig = plt.figure(figsize = figsize, dpi = dpi)
        gs = GridSpec(nrows, ncols, **gridspec_extra_args)
        ax = [fig.add_subplot(g) for g in gs]

        for a,d,e in zip(ax[:len(data)],data, error):
            a.plot(*d, linewidth=2)
            if errormode == 'traces':
                a.plot(*e, linewidth=1, linestyle='-', alpha=0.3)
            elif errormode == 'fill':
                for e_fill in e:
                    if len(e_fill) != 4:
                        raise ValueError('Error data is malformed for errormode fill')
                    if type(e_fill[3]) == dict:
                        a.fill_between(*e_fill[0:3], alpha=0.3, **e_fill[3])
                    else:
                        a.fill_between(*e_fill, alpha=0.3)
            else:
                raise ValueError('Unrecognized errormode')
        return fig, ax

    def contact_sheet_image(nrows, ncols, data, **gridspec_extra_args):
        """
        nrows = # of rows
        ncols = # of cols
        data = ( (img_data, {kwargs to imshow}) )
        """
        from matplotlib.gridspec import GridSpec
        fig = plt.figure()
        gs = GridSpec(nrows, ncols, **gridspec_extra_args)
        ax = [fig.add_subplot(g) for g in gs]
        for a,d in zip(ax, data):
            a.imshow(d[0], **d[1])
        return fig, ax

    def single_roi_summary(self,roinum, subplotshape=None):
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from matplotlib.figure import Figure
        from matplotlib.lines import Line2D
        from matplotlib.colors import LinearSegmentedColormap, colorConverter
        import seaborn as sns
        sns.set_style('dark')

        if self.settings['ROI set'] == 'rois':
            roi = self.rois[roinum]
        elif self.settings['ROI set'] == 'gridrois':
            roi = self.gridrois[roinum]

        figs = [[]]*3
        axs = [[]]*3

        # Display fovimg with yellow circle over ROI
        figs[0] = Figure(); FigureCanvas(figs[0])
        axs[0] = figs[0].add_subplot(111)
        axs[0].imshow(self.fovimg, vmin = np.percentile(self.fovimg, .25), vmax = np.percentile(self.fovimg, 99.75), cmap = plt.cm.gray, interpolation='nearest')
        axs[0].add_artist( roi.get_patch(linewidth=1, facecolor='none', edgecolor='y') )
        axs[0].set_xticks([])
        axs[0].set_yticks([])

        # Display zoom in on the ROI
        figs[1] = Figure(); FigureCanvas(figs[1])
        axs[1] = figs[1].add_subplot(111)
        pad = 20
        alpha_mask = LinearSegmentedColormap.from_list('alpha_mask', [ colorConverter.to_rgba('black', alpha=0.75), colorConverter.to_rgba('w',alpha=0) ], 2)
        try:
            startx = max(0, roi.x-pad)
            starty = max(0, roi.y-pad)
            endx = min(self.fovimg.shape[1], roi.x+roi.width+pad)
            endy = min(self.fovimg.shape[0], roi.y+roi.height+pad)
            axs[1].imshow( self.fovimg[ startx:endx ,
                                           starty:endy ] ,
                          cmap=plt.cm.gray,
                          interpolation='none' )
            axs[1].hold('on')
            axs[1].imshow( np.float64(roi.mask[ startx:endx ,
                                                starty:endy ]),
                          cmap=alpha_mask,
                          interpolation='none' )
        except AttributeError: # hack for not having to rewrite rois in hdf5 files for old experiments
            x = min([p[1] for p in roi.points])
            y = min([p[0] for p in roi.points])
            width = max([p[1] for p in roi.points]) - x
            height = max([p[0] for p in roi.points]) - y
            startx = max(0, x-pad)
            starty = max(0, y-pad)
            endx = min(self.fovimg.shape[1], x+width+pad)
            endy = min(self.fovimg.shape[0], y+height+pad)
            axs[1].imshow( self.fovimg[ startx:endx ,
                                           starty:endy ] ,
                          cmap=plt.cm.gray,
                          interpolation='none' )
            axs[1].hold('on')
            axs[1].imshow( np.float64(roi.mask[ startx:endx ,
                                                starty:endy ]) ,
                          cmap=alpha_mask,
                          interpolation='none' )
        #TODO add artist for this plot also
        axs[1].set_xticks([])
        axs[1].set_yticks([])

        # Plot individual stimuli
        if subplotshape is None:
            subplotshape = guess_subplotshape(len(self.unique_conditions))
        with sns.axes_style('darkgrid'):
            #figs[2] = Figure(); FigureCanvas(figs[2])
            #axs[2] = []
            #for n in range(subplotshape[0]*subplotshape[1]):
            #    axs[2].append(figs[2].add_subplot(subplotshape[0], subplotshape[1], n+1))
            figs[2], axs[2] = plt.subplots(*subplotshape, squeeze=False)
            for c in range(len(self.unique_conditions)):
                stim = self.unique_conditions[c]
                condition_mask = np.array([np.array_equal(x, stim[0]) for x in self.odors]) * np.array([np.array_equal(x, stim[1]) for x in self.spots])
                axs[2][np.unravel_index(c, subplotshape)].plot( np.nanmean(self.dff[roinum, np.atleast_1d(np.argwhere(condition_mask).squeeze()), :], axis=0).T )

                if (self.trialtype == 1) or (self.trialtype == 3): # Odor was on
                    axs[2][np.unravel_index(c, subplotshape)].add_line(Line2D((self.trialinfo['odor_pre_odor']-1,)*2, (-10,10), color='y', linestyle=':'))
                    axs[2][np.unravel_index(c, subplotshape)].add_line(Line2D((self.trialinfo['odor_pre_odor']-1+self.trialinfo['odor_on_odor'],)*2, (-10,10), color='y', linestyle=':'))
                if self.trialtype >= 2: # Light was on
                    axs[2][np.unravel_index(c, subplotshape)].add_line(Line2D((self.trialinfo['light_pre_light']-1,)*2, (-10,10), color='b', linestyle=':'))

                if self.trialtype == 1:
                    titlestr = 'Odor {0}'.format(self.unique_conditions[c][0])
                elif self.trialtype == 2:
                    titlestr = 'Stim {0}'.format(self.unique_conditions[c][1])
                elif self.trialtype == 3:
                    titlestr = 'Odor {0}, Stim {1}'.format(self.unique_conditions[c][0], self.unique_conditions[c][1])
                axs[2][np.unravel_index(c, subplotshape)].set_title(titlestr)
                axs[2][np.unravel_index(c, subplotshape)].set_xlim(left = 0, right = self.dff.shape[2])
                if c < subplotshape[1]*(subplotshape[0]-1):
                    axs[2][np.unravel_index(c, subplotshape)].set_xticks([])

        return figs, axs


    def single_roi_map(self, roinum, baseline_window, response_window, spots_to_plot = 'extrema', star_significant = False, vmin=None, vmax=None):
        import numpy as np
        import seaborn as sns
        from scipy.stats import t as tdist
        from matplotlib.patches import Ellipse, Rectangle, Polygon

        if self.settings['ROI set'] == 'gridrois':
            roi = expt.gridrois[roinum]
        else:
            roi = expt.rois[roinum]

        stimulus_mean = np.nanmean(self.dff[:,:,response_window[0]:response_window[1]], axis=2)

        mapsize = ( int(self.trialinfo['fos_height']/self.trialinfo['stimcluster_Spot size (px)']),
                    int(self.trialinfo['fos_width']/self.trialinfo['stimcluster_Spot size (px)']) )

        trialnums = [[]]*(np.prod(mapsize))
        for s in range(int(np.prod(mapsize))):
            trialnums[s] = np.nonzero(np.array([any(spots == s) for spots in self.spots]))

        spotmap = np.full( (max([x[0].shape[0] for x in trialnums]),) + mapsize, np.nan )
        for spot,trials in enumerate(trialnums):
            this_spot_means = stimulus_mean[roinum,trials].squeeze()
            spotmap[:,np.unravel_index(spot, mapsize)[0], np.unravel_index(spot, mapsize)[1]] = np.pad(this_spot_means, (0, spotmap.shape[0]-len(this_spot_means)), mode='constant', constant_values=(np.nan,))

        mp = np.nanmean(spotmap, axis=0)
        sd = np.nanstd(spotmap, axis=0, ddof=1)
        count = np.array([s[0].shape[0] for s in trialnums]).reshape(mapsize)

        tstat = mp/(sd / np.sqrt(count))
        p = tdist.cdf(tstat, count-1)
        wheresig = np.transpose((p > .95).transpose().nonzero())

        fig = plt.figure(figsize=(20,8))

        spotorder = np.argsort(mp.flatten())
        if spots_to_plot == 'biggest':
            spots_to_plot = spotorder[:-5:-1]
        elif spots_to_plot == 'smallest':
            spots_to_plot = spotorder[:4]
        elif spots_to_plot == 'extrema':
            spots_to_plot = np.hstack((spotorder[:-3:-1],spotorder[:2]))

        with sns.axes_style('dark'):
            ax0 = plt.subplot2grid( (2,2), (0,0) )
            ax1 = plt.subplot2grid( (2,2), (0,1) )
        with sns.axes_style('darkgrid'):
            ax2 = plt.subplot2grid( (2,4), (1,0) )
            ax3 = plt.subplot2grid( (2,4), (1,1) )
            ax4 = plt.subplot2grid( (2,4), (1,2) )
            ax5 = plt.subplot2grid( (2,4), (1,3) )
        ax = (ax0,ax1,ax2,ax3,ax4,ax5)

        #TODO: instead of this, use a system like for display_roi_responses where we pass in a normalize instance
        #      also find a better way of normalizing across rois
        if vmin is None:
            vmin = np.min(mp)
        if vmax is None:
            vmax = np.max(mp)
        # Plot the map
        m_im = ax[0].imshow(mp, interpolation='nearest', cmap=plt.cm.CMRmap, vmin = vmin, vmax = vmax)
        ax[0].set_xticks([])
        ax[0].set_yticks([])
        plt.colorbar(m_im, ax=ax[0])
        # Put stars on significant spots
        if star_significant:
            for s in wheresig:
                ax[0].annotate('*', xy = s)
        # Put letters on interesting spots
        letters = ('a', 'b', 'c', 'd')
        for s,l in zip(spots_to_plot, letters):
            ind = np.unravel_index(s, mp.shape)
            ax[0].annotate(l, xy = (ind[1], ind[0]),  fontsize=10, weight='bold', color='g', horizontalalignment='center', verticalalignment='center')
        # Plot the fov image
        p_im = ax[1].imshow(expt.fovimg, interpolation='nearest', cmap=plt.cm.gray)
        ax[1].set_xticks([])
        ax[1].set_yticks([])
        # Draw ROI on fov image
        ax[1].add_artist(roi.get_patch(linewidth=2, facecolor='none', edgecolor='y'))
        fig.suptitle('ROI {0}'.format(roinum), fontsize=18)
        # Put letters on subfigures
        for n in range(0,4):
            ax[n+2].plot(self.dff[roinum,trialnums[spots_to_plot[n]],baseline_window[0]:].squeeze().T)
            ax[n+2].hold('on')
            ax[n+2].plot(np.nanmean(self.dff[roinum,trialnums[spots_to_plot[n]],baseline_window[0]:], axis=1).T, linewidth=3, color='k')
            ax[n+2].text(0.02, .98, letters[n], weight='bold', horizontalalignment='left', verticalalignment='top', transform=ax[n+2].transAxes, color='k')
            if n == 0: ax[n+2].set_ylabel('$\Delta$F/f', fontsize=14)

        fig.subplots_adjust(wspace=.35, hspace=.25)

        return fig, ax, mp

    def map_single_roi(*args, **kwargs):
        from warnings import warn
        warn('map_single_roi renamed to single_roi_map')
        return single_roi_map(*args, **kwargs)


### Helper functions ###

def guess_subplotshape(n):
    """
    Attempts to guess a decent shape for a subplot grid. Finds the next largest
    perfect square. Use square root of this as the width, divide n by this to
    find the height
    """
    width = int(round(np.sqrt(np.ceil(np.sqrt(n))**2)))
    height = int(round(np.ceil(n/width)))
    return (height, width)

def display_lvrois(lvroi_file, roi_number_position='center'):
    import struct, os
    import readIJrois as rr

    # load image from lvroi file
    with open(lvroi_file, 'rb') as f: bytez = f.read()
    imgbytez = bytez[bytez.find(b'IMAGEDATA')+9:]
    try:
        from PIL import Image
        from io import BytesIO
        img = np.asarray(Image.open(BytesIO(imgbytez)))
        sz = img.shape
    except OSError:
        sz = struct.unpack('>2I',imgbytez[0:8])
        img = np.array(struct.unpack('>{0}B'.format(sz[0]*sz[1]), imgbytez[8:]))
        img.shape = sz

    rd = rr.RoiDecoder(sz)
    stimrois = rd.fromfile(lvroi_file, fileformat='labview')

    fig, ax = plt.subplots(1,1)
    ax.imshow(img, vmin = np.percentile(img, .25), vmax = np.percentile(img, 99.75), cmap = plt.cm.gray, interpolation='none')
    ells = []
    textpos = []
    for n,r in enumerate(stimrois):
        try:
            for subroi in r.subrois: ax.add_artist( subroi.get_patch(linewidth=1, facecolor='none', edgecolor='y') )
        except AttributeError:
            ax.add_artist( r.get_patch(linewidth=1, facecolor='none', edgecolor='y') )
        ax.add_artist( r.get_label(str(n), location=roi_number_position, fontsize=16, color='chartreuse', weight='bold', family='monospace', horizontalalignment='center', verticalalignment='center') )

    ax.set_xticks([])
    ax.set_yticks([])
    return fig, ax, stimrois

def shift_rois(stimrois, out_sz, in_zoom, out_zoom):
    from copy import deepcopy

    in_sz = stimrois[0].image_size

    size_ratio = [o/i for (o,i) in zip(out_sz, in_sz)]
    zoom_ratio = [o/i for (o,i) in zip(out_zoom, in_zoom)]

    ppz_in = [s/z for (s,z) in zip(in_sz, in_zoom)] # pixels per zoom
    ppz_out = [s/z for (s,z) in zip(out_sz, out_zoom)]

    startX = (out_zoom[0]-in_zoom[0])/2
    startY = (out_zoom[1]-in_zoom[1])/2
    endX = in_zoom[0] + (out_zoom[0]-in_zoom[0])/2
    endY = in_zoom[1] + (out_zoom[1]-in_zoom[1])/2

    startX_px = int(ppz_out[0]*startX)
    startY_px = int(ppz_out[1]*startY)
    endX_px = int(ppz_out[0]*endX)
    endY_px = int(ppz_out[1]*endY)

    def shift_single_roi(roi):
        shifted = deepcopy(roi)
        shifted.image_size = out_sz
        shifted.mask = np.zeros(out_sz, dtype=bool)

        if isinstance(roi, rr.RectRoi) or isinstance(roi, rr.OvalRoi):
            x = (roi.x-in_sz[0]/2)/ppz_in[0]*ppz_out[0] + out_sz[0]/2
            y = (roi.y-in_sz[1]/2)/ppz_in[1]*ppz_out[1] + out_sz[1]/2
            width = roi.width/ppz_in[0]*ppz_out[0]
            height = roi.height/ppz_in[1]*ppz_out[1]
            if isinstance(roi, rr.RectRoi):
                shifted = rr.RectRoi(x,y,width,height, image_size=out_sz)
            else:
                shifted = rr.OvalRoi(x,y,width,height, image_size=out_sz)
        # TODO: Make it so that mask is initialized properly for the following roi types
        #       It might require being smarter about params to __init__
        elif isinstance(roi, rr.ShapeRoi) or isinstance(roi, rr.PointRoi):
            for n,p in enumerate(roi.points):
                shifed.points[n] = ( (p[0]-in_sz[0]/2)/ppz_in[0]*ppz_out[0] + out_sz[0]/2,
                                     (p[1]-in_sz[1]/2)/ppz_in[1]*ppz_out[1] + out_sz[1]/2)
        elif isinstance(roi, rr.LineRoi) or isinstance(roi, rr.EllipseRoi):
            shifted.x1 = (roi.x1-in_sz[0]/2)/ppz_in[0]*ppz_out[0] + out_sz[0]/2
            shifted.y1 = (roi.y1-in_sz[1]/2)/ppz_in[1]*ppz_out[1] + out_sz[1]/2
            shifted.x2 = (roi.x2-in_sz[0]/2)/ppz_in[0]*ppz_out[0] + out_sz[0]/2
            shifted.y2 = (roi.y2-in_sz[1]/2)/ppz_in[1]*ppz_out[1] + out_sz[1]/2
        return shifted

    shifted_stimrois = []
    for roi in stimrois:
        try:
            subrois = []
            for subroi in roi.subrois:
                subrois.append(shift_single_roi(subroi))
            shifted_stimrois.append(rr.CompoundRoi(subrois, image_size=in_sz))
        except AttributeError:
            shifted_stimrois.append(shift_single_roi(roi))
    return shifted_stimrois

def new_gridrois(expt, h5set=None, roisz=20):
    """Overwrite gridrois in an Experiment with a new one"""
    import readIJrois as rr

    if h5set is None:
        h5set = expt.h5set
    else:
        import warnings
        warnings.warn(DeprecationWarning("Don't pass in an h5set. Function will now use the member h5set of expt"))

    sz = (expt.trialinfo['fov_x_res'], expt.trialinfo['fov_y_res'])
    width = np.floor(sz[0]/roisz)
    height = np.floor(sz[1]/roisz)
    x,y = np.meshgrid(np.arange(0,sz[0],width), np.arange(0, sz[1], height))
    gridrois = [rr.RectRoi(x,y,width,height,image_size=sz) for (x,y) in zip(x.flat,y.flat)]

    gridroi_data = np.zeros((len(gridrois), expt.h5set['data/img_data'].shape[0], expt.trialinfo['frameinfo_nframes']))

    # Iterate through trials
    filesz = 8*expt.trialinfo['fov_x_res']*expt.trialinfo['fov_y_res']*expt.trialinfo['frameinfo_nframes']
    nfiles = int(512*2**20 / filesz)
    inds = list(range(0, expt.h5set['data/img_data'].shape[0], nfiles))
    indexlist = []
    indices = list(range(expt.h5set['data/img_data'].shape[0]))
    for n,i in enumerate(inds[:-1]):
        indexlist.append(indices[inds[n]:inds[n+1]])
    indexlist.append(indices[inds[-1]:])

    chunkind = 0
    for (chunknum, inds) in enumerate(indexlist):
        gridroi_chunk = np.empty((len(gridrois), len(inds), expt.trialinfo['frameinfo_nframes']))

        for i, ind in enumerate(inds):
            try:
                #print('...{0} / {1}'.format(i+1, len(inds)))

                img = expt.h5set['data/img_data'][ind,:,:,:]

                for r, roi in enumerate(gridrois):
                    gridroi_chunk[r, i, :] = np.mean(img[:, roi.mask], axis=1)
            except Exception as err:
                with open(log_filename, 'a') as logfile:
                    error_handler(err, info_str, logfile)
                gridroi_nan = np.full((len(gridrois), expt.trialinfo['frameinfo_nframes']), np.nan)
                gridroi_chunk[:, fnum, :] = gridroi_nan

        try:
            gridroi_data[:,chunkind:(chunkind+len(inds)),:] = gridroi_chunk
        except Exception as err:
            with open(log_filename, 'a') as logfile:
                error_handler(err, info_str, logfile)
        finally:
            chunkind += len(inds)

    expt._gridroi_data = gridroi_data
    expt.gridrois = gridrois
    return gridrois

def display(fig):
    from IPython.core.display import display
    from matplotlib.backends.backend_agg import FigureCanvasAgg
    canvas = FigureCanvasAgg(fig)
    display(canvas.figure)
