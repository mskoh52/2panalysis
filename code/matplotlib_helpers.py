import matplotlib.pyplot as plt
from matplotlib import animation

def animate_stack(img, framerate=24, interpolation='nearest', cmap=plt.cm.gray):
    fig, ax = plt.subplots()
    im = plt.imshow(img[0,:,:], interpolation=interpolation, cmap=cmap)

    def update(fr):
        im.set_data(img[(fr % img.shape[0]),:,:])
        im.set_clim(img[(fr % img.shape[0]),:,:].min(), img[(fr % img.shape[0]),:,:].max())
        return im,
    anim = animation.FuncAnimation(fig, update, interval=1000/framerate)
    return anim
