import os, re, glob, datetime, csv, pickle, sys, logging
import numpy as np
import pandas as pd
import readIJrois as rr
from image_anal_helpers import *
import h5py

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('exptdir', help='Name of experiment within datadir')
    parser.add_argument('--max_mem', default='512M', help='Maximum memory to use for simultaneously loaded data files. Understands "K", "M", "G" for kilo, mega, giga. Default is "512M"')
    parser.add_argument('-d', '--datadir', help='Where to look for data. Overrides $DATADIR')
    parser.add_argument('--hfiledir', help='Where to save the hdf5 file. Overrides $HFILEDIR')
    parser.add_argument('-o', '--outputdir', help='Where to store files. Overrides $OUTPUTDIR')
    parser.add_argument('-l', '--log', default='./files_to_h5py_exptdir_yymmdd_hhmmss.log', help='Log file')
    args = parser.parse_args()

    exptdir = args.exptdir

    metric_prefixes = {'K': int(2**10),
                       'M': int(2**20),
                       'G': int(2**30)}
    m = re.match('([0-9]+)([A-Z]*)', args.max_mem)
    max_mem = int(m.group(1)) * metric_prefixes[m.group(2)]

    if ('DATADIR' in os.environ) and (os.environ['DATADIR'] != ''):
        datadir = os.environ['DATADIR']
    elif not(args.datadir is None):
        datadir = args.datadir
    else:
        raise NameError("datadir is not defined. --datadir option or $DATADIR environment variable must be set.")
    if ('HFILEDIR' in os.environ) and (os.environ['HFILEDIR'] != ''):
        hfiledir = os.environ['HFILEDIR']
    elif not(args.hfiledir is None):
        hfiledir = args.hfiledir
    else:
        raise NameError("hfiledir is not defined. --hfiledir option or $HFILEDIR environment variable must be set.")
    if ('OUTPUTDIR' in os.environ) and (os.environ['OUTPUTDIR'] != ''):
        outputdir = os.environ['OUTPUTDIR']
    elif not(args.outputdir is None):
        outputdir = args.outputdir
    else:
        raise NameError("outputdir is not defined. --outputdir option or $OUTPUTDIR environment variable must be set.")
else:
    raise Exception("run it from command line")

exptname = os.path.split(exptdir)[-1]
if args.log == './files_to_h5py_exptdir_yymmdd_hhmmss.log':
    logfile = os.path.join( os.path.dirname(os.path.realpath(__file__)),'logfiles/files_to_h5py_'+exptname+'_'+datetime.datetime.now().strftime('%y%m%d_%H%M%S')+'.log')
else:
    logfile = args.log

logger = logging.getLogger()
logger.setLevel(0)
formatter = logging.Formatter('{0} -- %(message)s'.format(exptname))
shandler = logging.StreamHandler()
shandler.setLevel(logging.DEBUG)
shandler.setFormatter(formatter)
fhandler = logging.FileHandler(logfile)
fhandler.setLevel(logging.INFO)
fhandler.setFormatter(formatter)
logger.addHandler(shandler)
logger.addHandler(fhandler)
logger.info(exptname)

exptdate = re.match('.*/([0-9]{6})', exptdir).group(1)
exptdate = datetime.date(int('20'+exptdate[0:2]), int(exptdate[2:4]), int(exptdate[4:6]))

if __debug__:
    hfile_path = os.path.join(hfiledir, exptdir+'hdf5_testing')
    import pdb
else:
    hfile_path = os.path.join(hfiledir, exptdir+'.hdf5')

os.makedirs(os.path.join(hfiledir, os.path.dirname(exptdir)), exist_ok=True)

if not(os.path.isdir(os.path.join(outputdir, exptdir))): os.makedirs(os.path.join(outputdir, exptdir))
filetree = list(os.walk(os.path.join(datadir, exptdir)))

with h5py.File(hfile_path, 'w') as hfile:
    for (dirnum, setdir) in enumerate(filetree[0][1]):
      logger.debug('{0}: {1} / {2}'.format(setdir,dirnum+1,len(filetree[0][1])))
      for ffov in os.listdir(os.path.join(datadir, exptdir, setdir)):
        try:

            match = re.search('(_*)(?P<fov>fov[0-9]+)(_*)', setdir)
            if match is None: continue # if folder name does not contain an fov number, skip to next folder
            fovname = match.group('fov')
            setname = setdir[:match.start()]+('_' if (match.group(1) and match.group(3)) else '')+setdir[match.end():]+'_'+ffov

            # Make list of filenames and validate them
            trialinfos = []
            filenames = []
            tmpfilenames = glob.glob(os.path.join(datadir, exptdir, setdir, ffov, '*green.raw'))
            tmpfilenames.sort()
            for f in tmpfilenames:
                trialinfo = load_trial_info(f)
                if len(trialinfo):
                    trialinfos.append(trialinfo)
                if os.stat(f).st_size != 0:
                    filenames.append(f)

            # Load information about the trials in this folder from first valid info file
            trialinfo = trialinfos[0]
            if len(trialinfo) == 0:
                continue
            if exptdate <= datetime.date(2015, 3, 18): trialinfo = convert_trialinfo_old_keys(trialinfo)

            # Create group for this folder if it doesn't already exist
            set_ = hfile.require_group(fovname+'/'+setname)
            meta = hfile.require_group(fovname+'/'+setname+'/meta')
            data = hfile.require_group(fovname+'/'+setname+'/data')

            for k in sorted(trialinfo.keys()):
                try:
                    set_.attrs[k] = trialinfo[k]
                except (ValueError, TypeError):
                    set_.attrs[k] = np.void(pickle.dumps(trialinfo[k]))
                except:
                    logger.warning('Exception while attempting to add attribute %s', k, exc_info=True)
                    logger.warning('k = %s', k)

            # Initialize variables
            spots = np.empty(len(filenames), dtype='object')
            for n in range(len(spots)): spots[n] = np.array([-1], dtype='int32')

            odors = np.full(len(filenames), -1, dtype='i4')

            glomstims = np.empty(len(filenames), dtype='object')
            for n in range(len(glomstims)): glomstims[n] = np.array([-1], dtype='int32')

            if trialinfo['frameinfo_use_odor']: # try to add odors
                if (type(trialinfo['odor_odors']) is np.float64) or (type(trialinfo['odor_odors']) is np.int32): # special case for only one glomstim
                    all_odors = [int(trialinfo['odor_odors'])]
                else:
                    all_odors = [int(x) for x in trialinfo['odor_odors'].split(',')]
            else:
                all_odors = []

            if trialinfo['frameinfo_stim_type'] == 'Glomstim':
                if type(trialinfo['stimcluster_Stimuli']) is list:
                    all_glomstims = trialinfo['stimcluster_Stimuli']
                elif (type(trialinfo['stimcluster_Stimuli']) is np.float64) or (type(trialinfo['stimcluster_Stimuli']) is np.int32): # special case for only one glomstim
                    all_glomstims = [[int(trialinfo['stimcluster_Stimuli'])]]
                else: #shouldn't ever happen
                    loggers.warning('writing weird value to stimcluster_Stimuli')
                    all_glomstims = [[int(x)] for x in trialinfo['stimcluster_Stimuli'].strip().split(' ')]
            else:
                all_glomstims = []

            if (trialinfo['frameinfo_stim_type'] == 'Glomstim') and trialinfo['frameinfo_use_odor'] and (len(all_odors) < len(all_glomstims)):
                for n in range( len(all_glomstims) - len(all_odors) ):
                    all_odors.append(-1)
            if (trialinfo['frameinfo_stim_type'] == 'Glomstim') and trialinfo['frameinfo_use_odor'] and (len(all_glomstims) < len(all_odors)) :
                for n in range( len(all_odors) - len(all_glomstims) ):
                    all_glomstims.append([-1])

            # Initialize datasets
            img_dataset = data.create_dataset('img_data',
                                              (len(filenames), trialinfo['frameinfo_nframes'], trialinfo['fov_y_res'], trialinfo['fov_x_res']),
                                              dtype='f8',
                                              compression='lzf',
                                              shuffle=True,
                                              fletcher32=True)
            img_dataset.dims[0].label = 'Trial'
            img_dataset.dims[1].label = 'Frame'
            img_dataset.dims[2].label = 'y'
            img_dataset.dims[3].label = 'x'

            # Iterate through files
            filesz = os.path.getsize(filenames[0])
            nfiles = max(1, int(max_mem / filesz))
            inds = list(range(0,len(filenames), nfiles))
            chunk_filelist = []
            for n,i in enumerate(inds[:-1]):
                chunk_filelist.append(filenames[inds[n]:inds[n+1]])
            chunk_filelist.append(filenames[inds[-1]:])

            chunkind = 0
            for chunknum, fnames in enumerate(chunk_filelist):
                logger.info('{0}: Chunk {1}/{2}'.format(setdir, chunknum+1, len(chunk_filelist)))

                img_chunk = np.empty((len(fnames), trialinfo['frameinfo_nframes'], trialinfo['fov_y_res'], trialinfo['fov_x_res']))

                for fnum, fname in enumerate(fnames):
                    if fnum % 5 == 0:
                        logger.info('{0}/{1}: Chunk {2}/{3}, file {4}/{5}'.format(fovname, setdir, chunknum+1, len(chunk_filelist), fnum+1, len(fnames)))
                    try:
                        trialinfo = load_trial_info(fname)
                        if __debug__:
                            if trialinfo['frameinfo_use_light']==0: continue
                        if exptdate <= datetime.date(2015, 3, 18): trialinfo = convert_trialinfo_old_keys(trialinfo)

                        if (type(trialinfo['light_spots']) is np.float64) or (type(trialinfo['light_spots']) is np.int32):
                            spots[chunkind+fnum] = np.array([trialinfo['light_spots']], dtype='int32')
                        elif len(trialinfo['light_spots']) > 0:
                            spots[chunkind+fnum] = np.array([int(x) for x in trialinfo['light_spots'].split(',')], dtype='int32')

                        if len(all_odors) > 0:
                            try:
                                odors[chunkind+fnum] = all_odors[int(trialinfo['frameinfo_trialnum'])]
                            except IndexError:
                                # if trial index out of range of odor list, then LabVIEW defaults to 0
                                odors[chunkind+fnum] = 0
                                if __debug__:
                                    pdb.set_trace()

                        if trialinfo['frameinfo_stim_type'] == 'Glomstim':
                            glomstims[chunkind+fnum] = spots[chunkind+fnum]
                            #glomstims[chunkind+fnum] = all_glomstims[int(trialinfo['frameinfo_trialnum'])]

                        with open(fname, 'rb') as fid:
                            img = np.fromfile(fid, '>d')
                        img.shape = (trialinfo['frameinfo_nframes'], trialinfo['fov_y_res'], trialinfo['fov_x_res'])
                        img_chunk[fnum, :, :, :] = img

                    except Exception as err:
                        info_str = '{0}/{1} >> Error while processing file {2}'.format(fovname, setname, os.path.basename(fname))
                        logger.warning(info_str, exc_info=True)
                        img_nan = np.full((trialinfo['frameinfo_nframes'], trialinfo['fov_y_res'], trialinfo['fov_x_res']), np.nan)
                        img_chunk[fnum, :, :, :] = img_nan

                try:
                    img_dataset[chunkind:(chunkind+len(fnames)),:,:,:] = img_chunk
                except Exception as err:
                    info_str = '{0}/{1} >> Error while writing chunk {2}'.format(fovname, setname, chunknum)
                    logger.warning(info_str, exc_info=True)
                finally:
                    chunkind += len(fnames)

            dt = h5py.special_dtype(vlen=np.dtype('int32'))
            if not('Spots' in set_):
                spotsset = meta.create_dataset('Spots', data=spots, dtype=dt)
            if not('Odors' in set_):
                odorsset = meta.create_dataset('Odors', data=odors)
            if not('Stimuli' in set_):
                glomstimsset = meta.create_dataset('GlomStimuli', data=glomstims, dtype=dt)
        except Exception as err:
            fovname = match.group('fov')
            setname = setdir[:match.start()]+('_' if (match.group(1) and match.group(3)) else '')+setdir[match.end():]+'_'+ffov
            info_str = '{0} >> Error while processing set {1}'.format(fovname, setname)
            logger.warning(info_str)
