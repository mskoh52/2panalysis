"""
Add gridrois to an hdf5 file to each set
Automatically splits field of view into a 10x10 grid
"""
import os, re, glob, pickle, datetime
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import seaborn as sns
import numpy as np
import pandas as pd
import csv
from PIL import Image
import readIJrois as rr
from image_anal_helpers import *
import tifffile
import h5py

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('exptdir', help='Name of experiment within datadir')
    parser.add_argument('--max_mem', default='512M', help='Maximum memory to use for simultaneously loaded data files. Understands "K", "M", "G" for kilo, mega, giga. Default is "512M"')
    parser.add_argument('-d', '--datadir', help='Where to look for data. Overrides $DATADIR')
    parser.add_argument('--hfiledir', help='Where to save the hdf5 file. Overrides $HFILEDIR')
    parser.add_argument('-o', '--outputdir', help='Where to store files. Overrides $OUTPUTDIR')
    parser.add_argument('-l', '--log', default='./add_gridrois_to_h5_exptdir_yymmdd_hhmmss_.log', help='Log file')
    args = parser.parse_args()
    exptdir = args.exptdir

    metric_prefixes = {'K': int(2**10),
                       'M': int(2**20),
                       'G': int(2**30)}
    m = re.match('([0-9]+)([A-Z]*)', args.max_mem)
    max_mem = int(m.group(1)) * metric_prefixes[m.group(2)]

    if ('DATADIR' in os.environ) and (os.environ['DATADIR'] != ''):
        datadir = os.environ['DATADIR']
    elif not(args.datadir is None):
        datadir = args.datadir
    else:
        raise NameError("datadir is not defined. --datadir option or $DATADIR environment variable must be set.")
    if ('HFILEDIR' in os.environ) and (os.environ['HFILEDIR'] != ''):
        hfiledir = os.environ['HFILEDIR']
    elif not(args.hfiledir is None):
        hfiledir = args.hfiledir
    else:
        raise NameError("hfiledir is not defined. --hfiledir option or $HFILEDIR environment variable must be set.")
    if ('OUTPUTDIR' in os.environ) and (os.environ['OUTPUTDIR'] != ''):
        outputdir = os.environ['OUTPUTDIR']
    elif not(args.outputdir is None):
        outputdir = args.outputdir
    else:
        raise NameError("outputdir is not defined. --outputdir option or $OUTPUTDIR environment variable must be set.")
else:
    raise Exception("run it from command line")

exptname = os.path.split(exptdir)[-1]
if args.log == './add_gridrois_to_h5_exptdir_yymmdd_hhmmss.log':
    log_filename = os.path.join( os.path.dirname(os.path.realpath(__file__)),'logfiles/add_gridrois_to_h5_'+exptname+'_'+datetime.datetime.now().strftime('%y%m%d_%H%M%S')+'.log')
else:
    log_filename = args.log

with open(log_filename, 'a') as logfile:
    logfile.write('--{0}--\n'.format(datetime.datetime.now()))
    logfile.write('--{0}--\n'.format(exptdir))

exptdate = re.match('.*/([0-9]{6})', exptdir).group(1)
exptdate = datetime.date(int('20'+exptdate[0:2]), int(exptdate[2:4]), int(exptdate[4:6]))

with h5py.File(os.path.join(hfiledir, exptdir+'.hdf5'), 'a') as hfile:
    for (fovnum, (fovname, fov)) in enumerate(hfile.items()):
        for (setnum, (setname, set_)) in enumerate(fov.items()):
            try:
                print('{0} ({1:d}/{2:d}): {3} ({4:d}/{5:d})'.format(fovname, fovnum+1, len(hfile.values()), setname, setnum+1, len(fov.items())))

                trialinfo = set_.attrs

                # Generate grid rois
                #sz = (trialinfo['fov_x_res'], trialinfo['fov_y_res'])
                sz = set_['data/registered_data'].shape[2:]
                aspect_ratio = sz[1] / sz[0]
                width = np.floor(sz[1]/20)
                height = np.floor(sz[0]/20)
                x,y = np.meshgrid(np.arange(0,sz[1], width), np.arange(0, sz[0], height))
                gridrois = [rr.RectRoi(x,y,width,height,image_size=sz) for (x,y) in zip(x.flat, y.flat)]

                if 'gridrois' in set_['meta']:
                    del set_['meta/gridrois']
                set_['meta'].create_dataset('gridrois', data=np.void(pickle.dumps(gridrois)))

                # Initialize datasets
                if 'gridroi_data' in set_['data']:
                    del set_['data/gridroi_data']
                gridroi_dataset = set_.create_dataset('data/gridroi_data',
                                                      (len(gridrois), set_['data/registered_data'].shape[0], trialinfo['frameinfo_nframes']),
                                                      dtype='d',
                                                      compression='lzf',
                                                      shuffle=True,
                                                      fletcher32=True)
                gridroi_dataset.dims[0].label = 'ROI'
                gridroi_dataset.dims[1].label = 'Trial'
                gridroi_dataset.dims[2].label = 'Frame'

                # Iterate through trials
                filesz = 8*trialinfo['fov_x_res']*trialinfo['fov_y_res']*trialinfo['frameinfo_nframes']
                print(filesz)
                nfiles = int(max_mem / filesz)
                inds = list(range(0, set_['data/registered_data'].shape[0], nfiles))
                print(inds)
                indexlist = []
                indices = list(range(set_['data/registered_data'].shape[0]))
                for n,i in enumerate(inds[:-1]):
                    indexlist.append(indices[inds[n]:inds[n+1]])
                indexlist.append(indices[inds[-1]:])

                chunkind = 0
                for (chunknum, inds) in enumerate(indexlist):
                    gridroi_chunk = np.empty((len(gridrois), len(inds), trialinfo['frameinfo_nframes']))

                    for i, ind in enumerate(inds):
                        try:
                            print('...{0} / {1}'.format(i+1, len(inds)))

                            img = set_['data/registered_data'][ind,:,:,:]

                            for r, roi in enumerate(gridrois):
                                gridroi_chunk[r, i, :] = np.mean(img[:, roi.mask], axis=1)
                        except Exception as err:
                            info_str = '{0}/{1}/{2} >> Error while processing trial {3}'.format(exptdir, fovname, setname, ind)
                            print(info_str)
                            with open(log_filename, 'a') as logfile:
                                error_handler(err, info_str, logfile)
                            gridroi_nan = np.full((len(gridrois), trialinfo['frameinfo_nframes']), np.nan)
                            gridroi_chunk[:, fnum, :] = gridroi_nan

                    try:
                        print('{0}/{1}/{2}: Writing chunk {3} / {4}'.format(exptdir, fovname, setname, chunknum+1, len(indexlist)))
                        gridroi_dataset[:,chunkind:(chunkind+len(inds)),:] = gridroi_chunk
                    except Exception as err:
                        info_str = '{0}/{1}/{2} >> Error while writing chunk {3}'.format(exptdir, fovname, setname, chunknum)
                        print(info_str)
                        with open(log_filename, 'a') as logfile:
                            error_handler(err, info_str, logfile)
                    finally:
                        chunkind += len(inds)
            except Exception as err:
                info_str = '{0}/{1}/{2} >> Error while processing set'.format(exptdir, fovname, setname)
                print(info_str)
                with open(log_filename, 'a') as logfile:
                    error_handler(err, info_str, logfile)
