# Contains little functions that don't fit anywhere else that are used
# by various other files

import csv, os, sys
import numpy as np
import pickle


# ---Initial data intake---

def load_trial_info(fname):
    """
    Reads the *_info files

    Parameters
    ----------
    fname: string
        Raw file name, e.g. 'FOV_0_rep_000_trial_0000_150420_162000_green.raw'

    Returns
    -------
    trialinfo: dict

    """
    # read info file
    trialinfo = dict()

    import re
    basename = re.match('.*FOV_[0-9]_rep_[0-9]{3}_trial_[0-9]{4}_[0-9]{6}_[0-9]{6}', fname).group()
    with open(basename+'_info', 'rb') as fid:
        fid.read(4)
        line0 = fid.readline().split(b'\t')
        line0 = [x.decode('utf-8').strip('\n') for x in line0]

    if line0[0] == 'VERSION': # Trialinfo files created after 150624 have VERSION field as first line
        if line0[1] == '150624' or line0[1] == '151001':
            with open(basename+'_info', 'rb') as fid:
                if os.stat(fid.name).st_size != int.from_bytes(fid.read(4), 'big')+4:
                    raise EOFError('{0}: actual file size does not match expected size'.format(fid.name))
                for n,f in enumerate(fid):
                    k = f[0:f.find(b'\t')] # tab is used to split key from value
                    k = k.decode('utf-8')

                    v = f[(f.find(b'\t')+1):]

                    if len(k) == 0: continue

                    # special cases
                    if k == 'stimcluster_ROI array':
                        v2 = fid.read(int.from_bytes(v[0:4], 'big') - len(v)+5)
                        v=v[4:]+v2[:-1]
                        pos=(fid.tell())
                        fid.seek(pos)
                    elif k == 'stimcluster_Stimuli':
                        stims = []
                        s = v.decode('utf-8')
                        for x in s.split(','):
                            try:
                                stims.append(int(x))
                            except ValueError:
                                if x == ' \n':
                                    pass
                                else:
                                    stims.append([int(y) for y in x.strip().split(' ')])
                            v = stims
                    else:
                        try:
                            if v.find(b'.') >= 0:
                                v = np.float64(v[:-1].decode('utf-8'))
                            else:
                                v = np.int32(v[:-1].decode('utf-8'))
                        except ValueError:
                            v = v[:-1].decode('utf-8')

                    trialinfo[k] = v

        else:
            raise ValueError('Unrecognized trialinfo version '+line0[1])

    else: # If the experiment was conducted before 150624
        with open(basename+'_info', 'r') as fid:
            reader = csv.reader(fid, delimiter='\t')
            for line in reader:
                if len(line) > 0:
                    try:
                        trialinfo[line[0]] = np.float64(line[1])
                    except ValueError:
                        trialinfo[line[0]] = line[1]
    return trialinfo

def convert_trialinfo_old_keys(trialinfo):
    """Before thinking to use versions in trialinfo files, I used a hacky way to fix older trialinfo files based on expt date
    """
    trialinfo_old_keys = dict([('height', 'fos_height'),
                               ('width', 'fos_width'),
                               ('x', 'fos_x'),
                               ('y', 'fos_y'),
                               ('acquire_flyback', 'fov_acquire_flyback'),
                               ('children', 'fov_children'),
                               ('name', 'fov_name'),
                               ('parent_tag', 'fov_parent_tag'),
                               ('pockels', 'fov_pockels'),
                               ('tag', 'fov_tag'),
                               ('tau', 'fov_tau'),
                               ('type', 'fov_type'),
                               ('x_res', 'fov_x_res'),
                               ('x_zoom', 'fov_x_zoom'),
                               ('y_res', 'fov_y_res'),
                               ('y_zoom', 'fov_y_zoom'),
                               ('z', 'fov_z'),
                               ('nframes', 'frameinfo_nframes'),
                               ('on_light', 'frameinfo_on_light'),
                               ('on_odor', 'frameinfo_on_odor'),
                               ('post_light', 'frameinfo_post_light'),
                               ('post_odor', 'frameinfo_post_odor'),
                               ('pre_light', 'frameinfo_pre_light'),
                               ('pre_odor', 'frameinfo_pre_odor'),
                               ('stim_type', 'frameinfo_stim_type'),
                               ('trialnum', 'frameinfo_trialnum'),
                               ('use_light', 'frameinfo_use_light'),
                               ('use_odor', 'frameinfo_use_odor'),
                               ('duty_cycle', 'light_duty_cycle'),
                               ('flash_delay', 'light_flash_delay'),
                               ('flash_duration', 'light_flash_duration'),
                               ('method', 'light_method'),
                               ('on_light', 'light_on_light'),
                               ('post_light', 'light_post_light'),
                               ('pre_light', 'light_pre_light'),
                               ('pulse_rate', 'light_pulse_rate'),
                               ('pulse_train_mode', 'light_pulse_train_mode'),
                               ('spots', 'light_spots'),
                               ('stim_every_n_frames', 'light_stim_every_n_frames'),
                               ('odors', 'odor_odors'),
                               ('on_odor', 'odor_on_odor'),
                               ('post_odor', 'odor_post_odor'),
                               ('odor_pre_odor', 'pre_odor'),
                               ('# active', 'stimcluster_# active'),
                               ('Spot size (px)', 'stimcluster_Spot size (px)')])
    new_trialinfo = dict()
    for item in trialinfo.items():
        try:
            new_trialinfo[trialinfo_old_keys[item[0]]] = item[1]
        except KeyError:
            new_trialinfo['???_'+item[0]] = item[1]
    return new_trialinfo

# ---Notebook helpers---

def treeprint(maxdepth=-1):
    """For use with h5py.Group.visit()"""
    def _treeprint(string):
        if string.count('/') <= maxdepth or maxdepth == -1:
            print('|--'*string.count('/')+string)
            return None
        else:
            return None
    return _treeprint

def leaf_count(obj):
    """For use with h5py.Group.visit()"""
    def is_leaf(obj):
        return (tuple(obj.keys()) == ('data', 'meta'))

    count = 0
    for k in obj.values():
        if is_leaf(k):
            count += 1
        else:
            count += leaf_count(k)
    return count

def summarize_fov(fov, datadir, exptdir):
    """Takes an fov and prints/returns info about its sets in order of acquisition during experiment

    Parameters
    ----------
    fov: an fov (an h5py group)

    Returns
    -------
    sorted_fovs: list
    start_times: list
    end_times: list
    """
    import re
    import time
    import tabulate
    from math import floor
    from functools import reduce
    from operator import add
    from glob import glob

    fovname = fov.name[1:]
    sets = list(fov.keys())

    startsecs = dict()
    starttimes = dict()
    endtimes = dict()
    for s in sets:
        fnames = glob(os.path.join(datadir, exptdir,fovname+'_'+s.replace('_FOV', '/FOV'), '*info'))
        secs = []
        hmss = []
        for f in fnames:
            hms = [int(x) for x in re.match('.*[0-9]{6}_([0-9]{2})([0-9]{2})([0-9]{2})_info', f).groups()]
            hmss.append(hms)
            secs.append( hms[0]*3600 + hms[1]*60 + hms[2] )

        startsecs[s] = min(secs)
        starttimes[s] = hmss[secs.index(min(secs))]
        endtimes[s] = hmss[secs.index(max(secs))]

    sortkeys = sorted(range(len(starttimes)), key=list(startsecs.values()).__getitem__)

    table = []
    sets = list(startsecs.keys())
    for n,s in enumerate(sortkeys):
        st = starttimes[sets[s]][0] * 3600 + starttimes[sets[s]][1] * 60 + starttimes[sets[s]][2]
        et = endtimes[sets[s]][0] * 3600 + endtimes[sets[s]][1] * 60 + endtimes[sets[s]][2]
        delta = et-st
        table.append( [n,
                       sets[s],
                       '{0:02d}:{1:02d}:{2:02d}'.format(*starttimes[sets[s]]),
                       '{0:02d}:{1:02d}:{2:02d}'.format(*endtimes[sets[s]]),
                       '{0:02d}:{1:02d}:{2:02d}'.format(floor(delta/3600), floor((delta%3600)/60), (delta%3600)%60),
                       fov[sets[s]]['data/img_data'].shape[0] ] )
    print(tabulate.tabulate(table, headers=('', 'Name', 'Start', 'End', 'Duration', '# trials')))
    return [t[1] for t in table], [t[2] for t in table], [t[3] for t in table]

def make_thresholding_filter(threshold):
    def conditionfilter(data):
        response = (data[:,:,(expt.light_response_period[0]):expt.light_response_period[1]]
                   - data[:,:,(expt.light_response_period[0]-3):expt.light_response_period[0]]).mean(axis=2)
        inds = np.argwhere(response > threshold).T
        data[inds[0], inds[1]] = np.nan
        return data
    return conditionfilter

with open('/home/mkoh/Documents/lab/analysis/2pAnalysis/completed_analysis/2015/09.September/150922_beads/px_per_15um_400x400', 'rb') as fid:
    final = pickle.load(fid)
from scipy.optimize import curve_fit
popt, pcov = curve_fit(lambda x,k: k*(1/x), np.array((1.,2.,3.,4.,5.)), final)
def um_per_pixel(zoom, res):
    return 7.5/(popt[0]*(1/zoom))*(400/res)
