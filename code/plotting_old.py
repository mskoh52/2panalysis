import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
import pickle

# change index vs. # of glomeruli

def display_rois(fovimg, rois, responses=None, cmap=plt.cm.CMRmap, norm=Normalize, ax=None):
    from matplotlib.patches import Ellipse, Rectangle, Polygon
    import readIJrois as rr
    if ax is None:
        with sns.axes_style('dark'):
            fig,ax = plt.subplots(1,1)

    ax.imshow(fovimg, vmin = np.percentile(fovimg, .25), vmax = 1.8*np.percentile(fovimg, 99.75), cmap = plt.cm.gray, interpolation='none')
    ells = []
    bgells = []
    centers = []
    if responses is None:
        colors = ['gold']*len(rois)
        edgecolors = ['y']*len(rois)
    else:
        colors = [cmap(norm(r)) for r in responses]
        edgecolors = [cmap(norm(r)) for r in responses]

    for r, c, ec in zip(rois, colors, edgecolors):
        if isinstance(r, rr.OvalRoi):
            ells.append(Ellipse(xy = (r.x+r.width/2, r.y+r.height/2), width=r.width, height=r.height, linewidth=1, facecolor='none', edgecolor=ec))
            bgells.append(Ellipse(xy = (r.x+r.width/2, r.y+r.height/2), width=16, height=16, facecolor=c, edgecolor='none', alpha=0.3))
            centers.append((r.x+r.width/2, r.y+r.height/2))
        elif isinstance(r, rr.RectRoi):
            ells.append(Rectangle(xy = (r.x+r.width/2, r.y+r.height/2), width=r.width, height=r.height, linewidth=1, facecolor='none', edgecolor=ec))
            bgells.append(Rectangle(xy = (r.x+r.width/2, r.y+r.height/2), width=16, height=16, facecolor=c, edgecolor='none', alpha=0.3))
            centers.append((r.x+r.width/2, r.y+r.height/2))
        elif isinstance(r, rr.FreeRoi):
            points = np.array(r.points)
            ells.append(Polygon(xy = points, linewidth=1, facecolor='none', edgecolor=ec))
            bgells.append(Polygon(xy = points, facecolor=c, edgecolor='none', alpha=0.3))
            centers.append(points.mean(axis=0))
        elif isinstance(r, rr.ShapeRoi):
            pass

    for (n, (e,b,c)) in enumerate(zip(ells, bgells, centers)):
        ax.add_artist(e)
        ax.add_artist(b)
        ax.text(c[0], c[1], str(n), fontsize=12, color='darkorange', weight='bold', family='monospace', horizontalalignment='center', verticalalignment='center')
    ax.set_xticks([])
    ax.set_yticks([])
    return (ax,colors)
