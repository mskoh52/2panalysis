import numpy as np
from numpy.linalg import svd

def princomp(data):
    """
    """

    U,s,V = svd(data, full_matrices=False)

    pcs = U*s^(1/2)
    eigs = s
    projections = np.dot(pcs.T, data)

    return (pcs,s,projections)
