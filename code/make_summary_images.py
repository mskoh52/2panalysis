import os, re, sys, glob, csv, datetime, logging
import h5py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
from image_anal_helpers import *
import tifffile

# Make avg images for each FOV
def make_summary_images(set_):

    trialinfo = set_.attrs

    if trialinfo['frameinfo_use_light']:
        baseline = tuple(int(x) for x in (max(0, trialinfo['light_pre_light']-10), trialinfo['light_pre_light']))
        stimulus = tuple(int(x) for x in (trialinfo['light_pre_light'], trialinfo['light_pre_light']+trialinfo['light_on_light']+3))
        post = tuple(int(x) for x in (trialinfo['light_pre_light']+trialinfo['light_on_light'], trialinfo['frameinfo_nframes']))
    elif trialinfo['frameinfo_use_odor']:
        baseline = tuple(int(x) for x in (max(0, trialinfo['odor_pre_odor']-10), trialinfo['odor_pre_odor']))
        stimulus = tuple(int(x) for x in (trialinfo['odor_pre_odor'], trialinfo['odor_pre_odor']+trialinfo['odor_on_odor']))
        post = tuple(int(x) for x in (trialinfo['odor_pre_odor']+trialinfo['odor_on_odor'], trialinfo['frameinfo_nframes']))
    else:
        baseline = tuple(int(x) for x in (0, trialinfo['frameinfo_nframes']))
        stimulus = tuple(int(x) for x in (0, trialinfo['frameinfo_nframes']))
        post = tuple(int(x) for x in (0, trialinfo['frameinfo_nframes']))

    try:
        set_avg_img = set_['data/img_data'][...].mean(axis=1)
        set_reg_img = set_['data/registered_data'][...].mean(axis=1)
    except MemoryError:
        from psutil import virtual_memory
        chunksz = round(virtual_memory().total/4)

        set_avg_img = np.zeros(set_['data/img_data'].shape[0,2,3])
        set_reg_img = np.zeros(set_['data/registered_data'].shape[0,2,3])

        n_avg_frames = chunksz/set_['data/img_data'].dtype.itemsize
        offset = 0
        while offset < set_['data/img_data'].shape[0]:
            start = offset
            stop = max(set_['data/img_data'], offset+n_avg_frames)
            set_avg_img[start:stop] = set_['data/img_data'][start:stop].mean(axis=1)
            offset += n_avg_frames

        n_reg_frames = chunksz/set_['data/registered_data'].dtype.itemsize
        offset = 0
        while offset < set_['data/registered_data'].shape[0]:
            start = offset
            stop = max(set_['data/registered_data'], offset+n_avg_frames)
            set_reg_img[start:stop] = set_['data/registered_data'][start:stop].mean(axis=1)
            offset += n_avg_frames

    base_img = np.nanmean(set_['data/registered_data'][:, baseline[0]:baseline[1],:,:], axis=1)
    stim_img = np.nanmean(set_['data/registered_data'][:, stimulus[0]:stimulus[1]+3,:,:], axis=1)
    set_ratio_img = (stim_img - base_img) / base_img

    return set_avg_img, set_reg_img, set_ratio_img


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('exptdir', help='Name of experiment within datadir')
    parser.add_argument('-d', '--datadir', help='Where to look for data. Overrides $DATADIR')
    parser.add_argument('--hfiledir', help='Where to save the hdf5 file. Overrides $HFILEDIR')
    parser.add_argument('-o', '--outputdir', help='Where to store files. Overrides $OUTPUTDIR')
    parser.add_argument('-l', '--log', default='./make_summary_images_exptdir_yymmdd_hhmmss.log', help='Log file')
    args = parser.parse_args()
    exptdir = args.exptdir
    if ('DATADIR' in os.environ) and (os.environ['DATADIR'] != ''):
        datadir = os.environ['DATADIR']
    elif not(args.datadir is None):
        datadir = args.datadir
    else:
        raise NameError("datadir is not defined. --datadir option or $DATADIR environment variable must be set.")
    if ('HFILEDIR' in os.environ) and (os.environ['HFILEDIR'] != ''):
        hfiledir = os.environ['HFILEDIR']
    elif not(args.hfiledir is None):
        hfiledir = args.hfiledir
    else:
        raise NameError("hfiledir is not defined. --hfiledir option or $HFILEDIR environment variable must be set.")
    if ('OUTPUTDIR' in os.environ) and (os.environ['OUTPUTDIR'] != ''):
        outputdir = os.environ['OUTPUTDIR']
    elif not(args.outputdir is None):
        outputdir = args.outputdir
    else:
        raise NameError("outputdir is not defined. --outputdir option or $OUTPUTDIR environment variable must be set.")

    exptname = os.path.split(exptdir)[-1]
    if args.log == './files_to_h5py_exptdir_yymmdd_hhmmss.log':
        logfile = os.path.join( os.path.dirname(os.path.realpath(__file__)),'logfiles/files_to_h5py_'+exptname+'_'+datetime.datetime.now().strftime('%y%m%d_%H%M%S')+'.log')
    else:
        logfile = args.log

    logger = logging.getLogger()
    logger.setLevel(0)
    formatter = logging.Formatter('{0} -- %(message)s'.format(exptname))
    shandler = logging.StreamHandler()
    shandler.setLevel(logging.DEBUG)
    shandler.setFormatter(formatter)
    fhandler = logging.FileHandler(logfile)
    fhandler.setLevel(logging.INFO)
    fhandler.setFormatter(formatter)
    logger.addHandler(shandler)
    logger.addHandler(fhandler)
    logger.info(exptname)

    exptdate = re.match('.*/([0-9]{6})', exptdir).group(1)
    exptdate = datetime.date(int('20'+exptdate[0:2]), int(exptdate[2:4]), int(exptdate[4:6]))

    with h5py.File(os.path.join(hfiledir, exptdir+'.hdf5'), 'r') as hfile:

        hfilename = os.path.basename(hfile.filename)[:-4]

        for (fovnum, (fovname, fov)) in enumerate(hfile.items()):

            try:
                fov_avg_img = np.zeros(list(fov.values())[0]['data/img_data'].shape[2:])

                for (setnum, (setname, set_)) in enumerate(fov.items()):
                    try:
                        logger.debug('{0}: Starting FOV {1}, set {2}'.format(hfilename, fovname, setname))
                        set_avg_img, set_reg_img, set_ratio_img = make_summary_images(set_)
                        fov_avg_img += set_avg_img.mean(axis=0)

                        match = re.search('FOV_[0-9]+', setname)
                        ffov = match.group()
                        setdir = fovname+'_'+setname[:match.start()-1] # get rid of FOV_0 in setname
                        if not(os.path.isdir(os.path.join(outputdir,exptdir,setdir,ffov))): os.makedirs(os.path.join(outputdir,exptdir,setdir,ffov))
                        tifffile.imsave(os.path.join(outputdir,exptdir,setdir,ffov, 'all_ratio.tif'), np.float32(set_ratio_img), photometric='minisblack')
                        tifffile.imsave(os.path.join(outputdir,exptdir,setdir,ffov, 'all_avg.tif'), np.float32(set_avg_img), photometric='minisblack')
                        tifffile.imsave(os.path.join(outputdir,exptdir,setdir,ffov, 'all_reg.tif'), np.float32(set_reg_img), photometric='minisblack')
                    except Exception as err:
                        info_str = '{0}/{1} >> Error while processing set {2}\n'.format(exptdir, fov.name, set_.name)
                        logger.warning(info_str, exc_info=True)

                fov_avg_img = fov_avg_img / set_['data/img_data'].shape[0]
                cmap_min = np.nanpercentile(fov_avg_img, .135)
                cmap_max = np.nanpercentile(fov_avg_img, 99.865)
                pil_img = Image.fromarray(255*((fov_avg_img - cmap_min)/(cmap_max-cmap_min)))
                pil_img.convert('L').save(os.path.join(outputdir,exptdir,fovname+'avg.tif'))

            except Exception as err:
                info_str = '{0} >> Error while processing fov {1}\n'.format(exptdir, fov.name)
                logger.warning(info_str, exc_info=True)

