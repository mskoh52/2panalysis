import os, sys
exptdir = sys.argv[1]
outputdir = '/home/mkoh/Documents/analysis/2pAnalysis/completed_analysis'
# make the roitree.json file
ls = os.listdir(os.path.join(outputdir, 'expts', os.path.basename(exptdir)))
import re, json
thedict = {}
for l in ls:
    m = re.match('(?P<fov>fov[0-9]+)_(?P<set>[a-zA-Z0-9_]+)_roi(?P<roi>[0-9]+)_traces.png', l)
    try:
        key1 = m.group('fov')
        key2 = m.group('set')
        val = m.group('roi')
        if key1 not in thedict:
            thedict[key1] = {}
        if key2 not in thedict[key1]:
            thedict[key1][key2] = []
        thedict[key1][key2].append(val)
    except AttributeError:
        pass
json.dumps(thedict)
with open(os.path.join(outputdir, 'expts', os.path.basename(exptdir), 'roitree.json'), 'w') as fid:
    json.dump(thedict, fid, indent=4)

