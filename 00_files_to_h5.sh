#!/bin/bash

for n in ${!folders[@]}
do
    python -O code/files_to_h5py.py --max_mem=2G ${folders[n]} &
done

wait

echo "=========== Done!!! ==========="
