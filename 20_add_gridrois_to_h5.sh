#!/bin/bash

for n in ${!folders[@]}
do
    python -O code/add_gridrois_to_h5.py ${folders[n]} &
done

wait

echo "=========== Done!!! ==========="
