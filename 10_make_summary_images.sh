#!/bin/bash

for n in ${!folders[@]}
do
    python code/make_summary_images.py ${folders[n]} &
done

wait

echo "=========== Done!!! ==========="
