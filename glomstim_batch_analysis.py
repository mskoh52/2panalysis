"""
Basic analysis for glomstim experiments
"""
import os, re, glob, pickle, datetime

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib.colors import Normalize

import plotting as pp
import seaborn as sns
import numpy as np
import readIJrois as rr
from image_anal_helpers import *
import h5py

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('exptdir', help='Name of experiment within datadir')
    parser.add_argument('--max_mem', default='512M', help='Maximum memory to use for simultaneously loaded data files. Understands "K", "M", "G" for kilo, mega, giga. Default is "512M"')
    parser.add_argument('-d', '--datadir', help='Where to look for data. Overrides $DATADIR')
    parser.add_argument('-o', '--outputdir', help='Where to store files. Overrides $OUTPUTDIR')
    parser.add_argument('-l', '--log', default='./glomstim_analysis_exptdir_yymmdd_hhmmss.log', help='Log file')
    args = parser.parse_args()
    exptdir = args.exptdir

    metric_prefixes = {'K': int(2**10),
                       'M': int(2**20),
                       'G': int(2**30)}
    m = re.match('([0-9]+)([A-Z]*)', args.max_mem)
    max_mem = int(m.group(1)) * metric_prefixes[m.group(2)]

    if ('DATADIR' in os.environ) and (os.environ['DATADIR'] != ''):
        datadir = os.environ['DATADIR']
    elif not(args.datadir is None):
        datadir = args.datadir
    else:
        raise NameError("datadir is not defined. --datadir option or $DATADIR environment variable must be set.")
    if ('OUTPUTDIR' in os.environ) and (os.environ['OUTPUTDIR'] != ''):
        outputdir = os.environ['OUTPUTDIR']
    elif not(args.outputdir is None):
        outputdir = args.outputdir
    else:
        raise NameError("outputdir is not defined. --outputdir option or $OUTPUTDIR environment variable must be set.")
else:
    raise Exception("run it from command line")

exptname = os.path.split(exptdir)[-1]
if args.log == './glomstim_analysis_exptdir_yymmdd_hhmmss.log':
    log_filename = os.path.join( os.path.dirname(os.path.realpath(__file__)),'logfiles/glomstim_analysis_'+exptname+'_'+datetime.datetime.now().strftime('%y%m%d_%H%M%S')+'.log')
else:
    log_filename = args.log

# load scalebar info
with open('/home/mkoh/Documents/analysis/2pAnalysis/completed_analysis/2015/09.September/150922_beads/px_per_15um_400x400', 'rb') as fid:
    final = pickle.load(fid)
    from scipy.optimize import curve_fit
    popt, pcov = curve_fit(lambda x,k: k*(1/x), np.array((1.,2.,3.,4.,5.)), final)
    um_per_pixel = lambda zoom, res: 7.5/(popt[0]*(1/zoom))*(400/res)

# make plots
with h5py.File(os.path.join(datadir, exptdir+'.hdf5'), 'r') as hfile:
    for fov in hfile.values():
        print(fov.name)
        sets = list(fov.keys())
        for s in sets:
            print(s)
            expt = pp.Experiment(fov[s])

            if expt.trialtype == 0: # No odor or light
                continue

            if expt.trialtype == 1: # Odor only experiment
                continue

            elif expt.trialtype == 2: # Light only experiment
                continue

            elif expt.trialtype == 3: # Odor + light experiment
                subplotshape = (np.int(np.ceil(len(expt.unique_conditions)/len(expt.unique_odors))), len(expt.unique_odors))
                basename = os.path.join(outputdir, 'expts', os.path.basename(exptdir), fov.name[1:]+'_'+s[:-6])

                ### Display stimuli ###
                try:
                    fig, ax, stimrois = pp.display_lvrois(os.path.join(datadir,exptdir,expt.trialinfo['stimroi_file']))
                    fig.set_size_inches(4,4)
                    fig.tight_layout(pad=.1)
                    fig.savefig(basename+'_stimrois.png', dpi=100, facecolor='white')
                except:
                    logwrite('Failed to make {0}_stimrois.png'.format(basename), log_filename)

                ### ROI response heatmap ###
                # Set the baseline period
                try:
                    original_baseline = expt.light_baseline_period
                    expt.light_baseline_period=(expt.light_response_period[0]-3,expt.light_response_period[0])
                    # Call it once to get the roi responses...TODO: find a less stupid way to do this
                    fig,ax,responses = expt.display_roi_responses()
                    norm = Normalize(vmin=responses.min(), vmax=0)
                    # Plot it for real
                    fig, ax, responses = expt.display_roi_responses(cmap=plt.cm.cubehelix, normalization=norm, subplotshape=subplotshape, print_roi_numbers=False)
                    # Restore the original value of light_baseline_period
                    expt.light_baseline_period=original_baseline
                    # Make it pretty and save it to disk
                    fig.set_size_inches(12,12)
                    fig.tight_layout(pad=.1)
                    fig.savefig(basename+'_roimap.png', dpi=100, facecolor='white')
                except:
                    logwrite('Failed to make {0}_roimap.png'.format(basename), log_filename)


                ### Single roi summary ###
                rois = expt.rois
                for n,r in enumerate(expt.rois):
                    try:
                        figs, axs = pp.Experiment.single_roi_summary(expt,n, subplotshape)
                        figs[0].set_size_inches(4,4)
                        figs[0].tight_layout(pad=.1)
                        figs[1].set_size_inches(3,3)
                        figs[1].tight_layout(pad=.1)
                        figs[2].set_size_inches(11, 3*subplotshape[0])
                        figs[2].tight_layout(pad=1.0)
                        figs[0].savefig(basename+'_roi{0:02d}_fovimg.png'.format(n), dpi=100, facecolor='white')
                        figs[1].savefig(basename+'_roi{0:02d}_roiimg.png'.format(n), dpi=100, facecolor='white')
                        figs[2].savefig(basename+'_roi{0:02d}_traces.png'.format(n), dpi=100, facecolor='white')
                    except:
                        logwrite('Failed to make {0}_roi{1:02d}_blahblah.png'.format(basename, n), log_filename)
                plt.close('all')
                del fig
                del figs

# make the roitree.json file
ls = os.listdir(os.path.join(outputdir, 'expts', os.path.basename(exptdir)))
import re, json
thedict = {}
for l in ls:
    m = re.match('(?P<fov>fov[0-9]+)_(?P<set>[a-zA-Z0-9_]+)_roi(?P<roi>[0-9]+)_traces.png', l)
    try:
        key1 = m.group('fov')
        key2 = m.group('set')
        val = m.group('roi')
        if key1 not in thedict:
            thedict[key1] = {}
        if key2 not in thedict[key1]:
            thedict[key1][key2] = []
        thedict[key1][key2].append(val)
    except AttributeError:
        pass
json.dumps(thedict)
with open(os.path.join(outputdir, 'expts', os.path.basename(exptdir), 'roitree.json'), 'w') as fid:
    json.dump(thedict, fid, indent=4)
